#version 410 core

uniform mat4 MVPMatrix;
uniform mat4 NormalMatrix;
uniform mat4 ModelViewMatrix;

uniform vec4 Color;

in vec2 in_UV;
in vec3 in_Position;
in vec3 in_Tangent;
in vec3 in_Normal;
in vec4 in_Color;

out vec4 pass_Color;
out vec3 pass_Position;
out vec2 pass_UV;
out vec3 passLightDirections[4];
out vec3 passLightPosition[4];

struct Light {
    vec4 typeRangeSpotAngleIntensity;
    vec4 direction;
    vec4 position;
    vec4 color;
};

layout (shared) uniform Lights {
    int numberOfLights;
    Light light[4];
};

void main(void) {
	gl_Position = MVPMatrix * vec4(in_Position, 1.0);
	pass_Color = in_Color * Color;
	pass_UV = in_UV;

    vec3 norm = (NormalMatrix * vec4(in_Normal, 0.0)).xyz;
    vec3 tangent = normalize((NormalMatrix * vec4(in_Tangent, 0.0)).xyz);

	vec3 binormal = normalize(cross(norm, tangent));
	mat3 toObjectLocal = mat3(
	    tangent.x, binormal.x, norm.x,
	    tangent.y, binormal.y, norm.y,
	    tangent.z, binormal.z, norm.z);
	pass_Position = toObjectLocal * (ModelViewMatrix * vec4(in_Position, 1.0)).xyz;

    for (int i=0; i<numberOfLights; i++) {
        passLightDirections[i] = normalize(toObjectLocal * light[i].direction.xyz);
    }

}