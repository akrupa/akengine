#version 410 core

uniform mat4 MVPMatrix;
uniform mat4 NormalMatrix;
uniform mat4 ModelViewMatrix;

uniform vec4 Color;

in vec3 in_Position;
in vec3 in_Normal;
in vec4 in_Color;
in vec2 in_UV;

out vec3 pass_Normal;
out vec4 pass_Color;
out vec4 pass_Position;
out vec2 pass_UV;

void main(void) {
	gl_Position = MVPMatrix * vec4(in_Position, 1.0);
	pass_Normal = (NormalMatrix * vec4(in_Normal, 0.0)).xyz;
	pass_Color = in_Color*Color;
    pass_UV = in_UV;
    pass_Position = ModelViewMatrix * vec4(in_Position, 1.0);
}