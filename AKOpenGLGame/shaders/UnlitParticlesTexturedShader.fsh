#version 410 core

#define M_PI 3.1415926535897932384626433832795

in vec4 pass_Color;
in vec2 pass_UV;

layout(location = 0) out vec4 out_Color;

uniform sampler2D texture0;

void main(void) {

    float A=1.0f;
    float a=20.0f;
    float c=20.0f;
    float b=0.0f;
    float xy = A * exp(-(a*(pass_UV.x-0.5f)*(pass_UV.x-0.5f)+2.0f*b*(pass_UV.x-0.5f)*(pass_UV.y-0.5f) + c*(pass_UV.y-0.5f)*(pass_UV.y-0.5f))+1.0f);
    vec4 col = vec4(xy, xy, xy,1.0f);
	out_Color = pass_Color*col;
	//out_Color = pass_Color * texture( texture0, pass_UV );
}