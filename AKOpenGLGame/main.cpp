//
//  main.cpp
//  AKOpenGLGame-OSX
//
//  Created by Adrian Krupa on 15.05.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <Engine.h>
#include <SDL2/SDL_main.h>

#include <iostream>

int main(int argc, char *argv[]) {
    AKOpenGLEngine::Engine *engine = new AKOpenGLEngine::Engine();
    engine->CreateWindow();
    engine->Run();
    
    
    // insert code here...
    std::cout << "Hello, World2!\n";
    return 0;
}
