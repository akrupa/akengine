//
//  Renderer.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 15.05.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <iostream>

#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
#include <OpenGLES/ES3/glext.h>
#else
#include <GL/glew.h>
#endif
#include <SDL2/SDL_image.h>

#include "Renderer.h"
#include "Utilities/GLDebug.h"
#include "Scene.h"
#include "Managers/InputManager.h"
#include "Managers/TextureManager.h"
#include "Managers/RendererManager.h"

INITIALIZE_EASYLOGGINGPP

namespace AKOpenGLEngine {
    
    void Renderer::CreateDebugPCContext() {
        
#if defined(__APPLE__)
        int minor = -1;
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
#else
        int minor = 6;
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
#endif
        SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
        
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);
        
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
        
        while (!maincontext&& minor + 1 >= 0) {
            if (mainwindow) {
                SDL_DestroyWindow(mainwindow);
            }
            mainwindow = SDL_CreateWindow("AKOpenGLEngine", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                          1000, 800, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE | SDL_WINDOW_HIDDEN);
            maincontext = SDL_GL_CreateContext(mainwindow);
            minor--;
        }
        if (!mainwindow){
            std::cout << "SDL Error: " << SDL_GetError() << std::endl;
            SDL_Quit();
            return;
        }
        
        
#if !TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR
        
        glewExperimental = GL_TRUE;
        GLenum err = glewInit();
        if (GLEW_OK != err)
        {
            fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
        }
#endif
        
        GL_CHECK_CLEAR();
    }
    
    void Renderer::CreateDebugContext() {
        InitLogger();
        if (SDL_Init(SDL_INIT_VIDEO) != 0) {
            fprintf(stderr,
                    "\nUnable to initialize SDL:  %s\n",
                    SDL_GetError()
                    );
            return;
        }
        
#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
        //CreateMobileWindow();
#else
        CreateDebugPCContext();
#endif
        
    }
    
    void Renderer::DestroyWindow() {
        SDL_GL_DeleteContext(maincontext);
        SDL_DestroyWindow(mainwindow);
        SDL_Quit();
    }
    
    void Renderer::CreateWindow() {
        InitLogger();

        if (SDL_Init(SDL_INIT_VIDEO) != 0) {
            fprintf(stderr,
                    "\nUnable to initialize SDL:  %s\n",
                    SDL_GetError()
                    );
            return;
        } else {
#if defined(_DEBUG)
            std::cout << "Initialized SDL" << std::endl;
#endif
        }
        
#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
        CreateMobileWindow();
#else
        CreatePCWindow();
#endif
        
        int w;
        int h;
        SDL_GL_GetDrawableSize(mainwindow, &w, &h);
        
        currentScene = new Scene();
        currentScene->InitBasicScene();
 
        currentScene->SetCamerasAspect(w, h);
        
        GLint defaultFBO;
        GL_CHECK(glGetIntegerv(GL_FRAMEBUFFER_BINDING, &defaultFBO));
        RendererManager::getInstance().setDefaultFramebuffer(defaultFBO);

        
        int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
        if( !( IMG_Init( imgFlags ) & imgFlags ) ) {
            LOG(ERROR) << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError();
        }
    }
    
    void Renderer::CreateMobileWindow() {
        int major = 3;
        
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);
        
        while (!maincontext && major >= 2) {
            if (mainwindow) {
                SDL_DestroyWindow(mainwindow);
            }
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, major);
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
            SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES );
            
            SDL_DisplayMode displayMode;
            SDL_GetCurrentDisplayMode(0, &displayMode);
            
            mainwindow = SDL_CreateWindow(NULL, 0, 0, displayMode.w, displayMode.h, SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_FULLSCREEN);
            maincontext = SDL_GL_CreateContext(mainwindow);
            major--;
        }
        if (!mainwindow){
            std::cout << "SDL Error: " << SDL_GetError() << std::endl;
            SDL_Quit();
            return;
        }
        
        GL_CHECK_CLEAR();
        GL_CHECK_RETURN(LOG(INFO) << glGetString(GL_VENDOR));
        GL_CHECK_RETURN(LOG(INFO) << glGetString(GL_VERSION));
        GL_CHECK_RETURN(LOG(INFO) << glGetString(GL_RENDERER));
        GL_CHECK_RETURN(LOG(INFO) << glGetString(GL_SHADING_LANGUAGE_VERSION));
        
    }
    
    void Renderer::CreatePCWindow() {
        
#if defined(__APPLE__)
        int minor = -1;
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
#else
        int minor = 6;
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
#endif
        SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
        
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);
        
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
        
        while (!maincontext&& minor + 1 >= 0) {
            if (mainwindow) {
                SDL_DestroyWindow(mainwindow);
            }
            mainwindow = SDL_CreateWindow("AKOpenGLEngine", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                          1000, 800, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE );
            maincontext = SDL_GL_CreateContext(mainwindow);
            minor--;
        }
        if (!mainwindow){
            std::cout << "SDL Error: " << SDL_GetError() << std::endl;
            SDL_Quit();
            return;
        }
        
        
#if !TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR
       
        glewExperimental = GL_TRUE;
        GLenum err = glewInit();
        if (GLEW_OK != err)
        {
            fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
        }
#endif
        
        GL_CHECK_CLEAR();
        GL_CHECK_RETURN(LOG(INFO) << glGetString(GL_VENDOR));
        GL_CHECK_RETURN(LOG(INFO) << glGetString(GL_VERSION));
        GL_CHECK_RETURN(LOG(INFO) << glGetString(GL_RENDERER));
        GL_CHECK_RETURN(LOG(INFO) << glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        if (outputVideoToFile) {
            const char* cmd = "/usr/local/bin/ffmpeg -loglevel panic -r 30 -f rawvideo -pix_fmt rgba -s 1000x800 -i - " " -threads 0 -preset fast -y -crf 21 -vf vflip output.mp4 >> /dev/null";
            ffmpeg = popen(cmd, "r+");
        }

    }
    
    void Renderer::ProcessEvent(const SDL_Event * event) {
        InputManager::getInstance().processSDLEvents((void*)event);

        if (event->type == SDL_WINDOWEVENT) {
            switch (event->window.event) {
                case SDL_WINDOWEVENT_RESIZED:
                case SDL_WINDOWEVENT_MOVED:
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                    int w;
                    int h;
                    SDL_GL_GetDrawableSize(mainwindow, &w, &h);
                    currentScene->SetCamerasAspect(w, h);
                    break;
            }
        }
    }

    void Renderer::Run() {
        GL_CHECK_CLEAR();
        bool quit=false;
        SDL_Event event;
        SDL_GL_MakeCurrent(mainwindow, maincontext);
        //SDL_GL_SetSwapInterval(0);
        while (!quit) {
            while( SDL_PollEvent( &event ) ){
                if( event.type == SDL_QUIT ){
                    quit = true;
                }
                ProcessEvent(&event);
            }
            UpdateAndDraw();

            SDL_GL_SwapWindow(mainwindow);
            if (outputVideoToFile) {
                int width = 1000;
                int height = 800;
                int* buffer = new int[width*height];
                GL_CHECK(glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer));
                
                fwrite(buffer, sizeof(int)*width*height, 1, ffmpeg);
                delete []buffer;
            }
        }
        currentScene->OnDestroy();
        SDL_GL_DeleteContext(maincontext);
        SDL_DestroyWindow(mainwindow);
        SDL_Quit();
        if (outputVideoToFile) {
            pclose(ffmpeg);
        }

    }

    void Renderer::UpdateAndDraw() {
        renderFrames++;
        
        auto currentTime = SDL_GetTicks();
        auto elapsedTimeFromLastFPSLogTime = currentTime - lastFPSLogTime;
        auto deltaTime = currentTime - lastUpdateDrawTime;
        
        fixedDeltaTimeToUpdate += deltaTime;
        //while (fixedDeltaTimeToUpdate > fixedDeltaTime) {
            currentScene->FixedUpdate(0.016);
            fixedDeltaTimeToUpdate -= fixedDeltaTime;
        //}
        
        auto startUpdateTime = SDL_GetTicks();
        currentScene->Update((float) deltaTime/1000.0f);
        auto endUpdateTime = SDL_GetTicks();
        
        auto startRenderTime = SDL_GetTicks();
        currentScene->Render();
        auto endRenderTime = SDL_GetTicks();
        
        renderClock += endRenderTime - startRenderTime;
        updateClock += endUpdateTime - startUpdateTime;
        
        if (elapsedTimeFromLastFPSLogTime > 5000) {
            printf("Frame render time: %lf ms\n", renderClock * 1000.0 / renderFrames);
            printf("Frame update time: %lf ms\n", updateClock * 1000.0 / renderFrames);
            printf("FPS: %lf\n", renderFrames * 1000.0 / elapsedTimeFromLastFPSLogTime);
            //TextureManager::getInstance().SaveTextureToPNG("texture.bmp", 256, 256, 1);
            renderFrames = 0;
            renderClock = 0;
            updateClock = 0;
            lastFPSLogTime = currentTime;
        }
        lastUpdateDrawTime = currentTime;
        
    }
    
    void Renderer::GetWindowSize(int *width, int *height) {
        SDL_GetWindowSize(mainwindow, width, height);
    }
    
    void Renderer::InitLogger() {
        el::Configurations defaultConf;
        defaultConf.setToDefault();
        defaultConf.setGlobally(el::ConfigurationType::Format, "%level %fbase::%line: %msg");
        el::Loggers::reconfigureLogger("default", defaultConf);
    }
}

