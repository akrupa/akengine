//
//  Texture.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 07.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Texture_H_
#define __Texture_H_

#include <string>
#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
#include <OpenGLES/ES3/glext.h>
#else
#include <GL/glew.h>
#endif

namespace YAML {
    class Emitter;
}

namespace AKOpenGLEngine {

    class Texture {
        
        std::string name;
        GLenum image_format;
        GLenum internal_format;
        GLint level;
        GLint border;
        
    protected:
        int bind;

    public:
        Texture(int bind, std::string name, GLenum image_format, GLenum internal_format, GLint level, GLint border);
        
        std::string GetName() const;

        int GetBind() const;
        GLenum GetImageFormat() const;
        GLenum GetInternalFormat() const;

        friend YAML::Emitter &operator<<(YAML::Emitter &out, const Texture *t);
    };
}


#endif //__Texture_H_
