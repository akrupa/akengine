//
//  RenderTexture.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 02.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __AKOpenGLEngine__RenderTexture__
#define __AKOpenGLEngine__RenderTexture__

#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
#include <OpenGLES/ES3/glext.h>
#else
#include <GL/glew.h>
#endif

#include "Texture.h"

namespace AKOpenGLEngine {
    
    class RenderTexture : public Texture {
        
        GLuint renderTextureBind;
        GLuint framebuffer;
        GLuint depthRenderbuffer;
        
        GLuint width, height;
        
        bool fixedPixelSize = true;
        
    public:
        RenderTexture(GLuint width = 256, GLuint height = 256);
        
        void GetSizeOfTexture(GLuint *width, GLuint *height);
        GLuint GetFramebuffer();
        
    };
}

#endif /* defined(__AKOpenGLEngine__RenderTexture__) */
