//
//  RenderTexture.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 02.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "RenderTexture.h"
#include "../Utilities/GLDebug.h"

namespace AKOpenGLEngine {
    
    RenderTexture::RenderTexture(GLuint width, GLuint height) : Texture(0, "", GL_RGB, GL_RGB, 0, 0) {
        this->width = width;
        this->height = height;

        GL_CHECK(glGenFramebuffers(1, &framebuffer));
        GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, framebuffer));
        
        GL_CHECK(glGenTextures(1, &renderTextureBind));
        GL_CHECK(glBindTexture(GL_TEXTURE_2D, renderTextureBind));
        GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0));
        
        GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
        GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
        
        GL_CHECK(glGenRenderbuffers(1, &depthRenderbuffer));
        GL_CHECK(glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer));
        GL_CHECK(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height));
        GL_CHECK(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer));
        
        GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, renderTextureBind, 0));

        GL_CHECK_RETURN(auto status = glCheckFramebufferStatus(GL_FRAMEBUFFER));
        bind = renderTextureBind;
        
        
        if (status != GL_FRAMEBUFFER_COMPLETE) {
            return;
        }
        
        GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0));
    }
    
    void RenderTexture::GetSizeOfTexture(GLuint *width, GLuint *height) {
        *width = this->width;
        *height = this->height;
    }
    
    GLuint RenderTexture::GetFramebuffer() {
        return framebuffer;
    }
}