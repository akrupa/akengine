//
//  CubeMapTexture.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 11.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "CubeMapTexture.h"

namespace AKOpenGLEngine {
    
    CubeMapTexture::CubeMapTexture(int bind, std::string name, GLenum image_format) : Texture(bind, name, image_format, 0, 0, 0) {
    }
    
}
