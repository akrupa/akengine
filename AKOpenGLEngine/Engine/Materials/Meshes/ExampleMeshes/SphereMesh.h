//
//  SphereMesh.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __SphereMesh_H_
#define __SphereMesh_H_

#include <map>
#include <string>

#include "../TrianglesMesh.h"

namespace AKOpenGLEngine {
    class SphereMesh : public TrianglesMesh {

    public:
        SphereMesh(unsigned int density = 40, glm::vec4 const& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
        SphereMesh(std::map<std::string, float> const& parameters);
    };
}


#endif //__SphereMesh_H_
