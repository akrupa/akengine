//
//  CylinderMesh.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "CylinderMesh.h"
#include "../Triangle.h"

using namespace glm;

namespace AKOpenGLEngine {

    CylinderMesh::CylinderMesh(float R, float height, unsigned int density, vec4 const& color) {
        name = "CylinderMesh";
        Vertex v;
        v.normal = vec3(0.0f, 1.0f, 0.0f);
        v.color = color;

        for (unsigned int i = 0; i <= density; ++i) {
            float alpha = i * (float) M_PI * 2 / density;
            for (int j = 0; j < 2; j++) {
                v.position = vec3(
                        R * sin(alpha),
                        (j * 2 - 1) * height / 2,
                        R * cos(alpha));
                v.normal = vec3(
                        sin(alpha),
                        0,
                        cos(alpha));
                v.UV = vec2((float) i / (float) density, j);
                vertices.push_back(v);
            }
        }

        for (int j = 0; j < 2; j++) {
            v.position = vec3(0, (j * 2 - 1) * height / 2, 0);
            v.normal = vec3(0, (j * 2 - 1), 0);
            v.UV = vec2(0.5, 0.5);
            vertices.push_back(v);
            for (unsigned int i = 0; i <= density; ++i) {
                float alpha = i * (float) M_PI * 2 / density;
                v.position = vec3(
                        R * sin(alpha),
                        (j * 2 - 1) * height / 2,
                        R * cos(alpha));
                v.UV = vec2(sin(alpha), cos(alpha));
                vertices.push_back(v);
            }
        }


        Triangle t;
        for (unsigned int i = 0; i < density; ++i) {

            t.indices.a = i * 2;
            t.indices.b = (i + 1) * 2;
            t.indices.c = i * 2 + 1;
            triangles.push_back(t);

            t.indices.a = (i + 1) * 2;
            t.indices.b = (i + 1) * 2 + 1;
            t.indices.c = i * 2 + 1;
            triangles.push_back(t);
        }
        for (unsigned int j = 0; j < 2; j++) {
            unsigned int offset =  2*(density+1) + j*(density+2);
            for (unsigned int i = 0; i <= density; ++i) {
                t.indices.a = offset;
                t.indices.b = offset +i%(density*2);
                t.indices.c = offset+(i+1)%(density*2);
                if(j==0) {
                    unsigned int temp = t.indices.a;
                    t.indices.a = t.indices.c;
                    t.indices.c = temp;
                }
                triangles.push_back(t);
            }
        }

    }
}