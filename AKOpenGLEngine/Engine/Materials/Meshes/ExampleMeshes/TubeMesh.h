//
//  TubeMesh.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __TubeMesh_H_
#define __TubeMesh_H_

#include "../TrianglesMesh.h"

namespace AKOpenGLEngine {

    class TubeMesh : public TrianglesMesh {

    public:
        TubeMesh(float R=1.0f, float r=0.5f, float height =2.0f, unsigned int density = 40, glm::vec4 const& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
    };
}


#endif //__TubeMesh_H_
