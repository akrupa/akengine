//
//  TubeMesh.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "TubeMesh.h"
#include "../Triangle.h"

using namespace glm;

namespace AKOpenGLEngine {
	TubeMesh::TubeMesh(float R, float r, float height, unsigned int density, glm::vec4 const& color) : TrianglesMesh()  {
        name = "TubeMesh";
        Vertex v;
        v.color = color;
        v.normal = vec3(0.0f, 1.0f, 0.0f);
        unsigned int offset = 0;

        for (unsigned int i = 0; i <= density; ++i) {
            float alpha = i * (float) M_PI * 2 / density;
            for (int j = 0; j < 2; j++) {
                v.position = vec3(
                        R * sin(alpha),
                        (j * 2 - 1) * height / 2,
                        R * cos(alpha));
                v.normal = vec3(
                        sin(alpha),
                        0,
                        cos(alpha));
                v.UV = vec2((float) i / (float) density, j);
                vertices.push_back(v);
            }
        }

        for (unsigned int i = 0; i <= density; ++i) {
            float alpha = i * (float) M_PI * 2 / density;
            for (int j = 0; j < 2; j++) {
                v.position = vec3(
                        r * sin(alpha),
                        (j * 2 - 1) * height / 2,
                        r * cos(alpha));
                v.normal = vec3(
                        -sin(alpha),
                        0,
                        -cos(alpha));
                v.UV = vec2((float) i / (float) density, j);
                vertices.push_back(v);
            }
        }

        for (int j = 0; j < 2; j++) {
            v.normal = vec3(0, (j * 2 - 1), 0);
            for (unsigned int i = 0; i <= density; ++i) {
                float alpha = i * (float) M_PI * 2 / density;
                v.position = vec3(
                        R * sin(alpha),
                        (j * 2 - 1) * height / 2,
                        R * cos(alpha));
                v.UV = vec2(0.5f+sin(alpha), 0.5f+cos(alpha));
                vertices.push_back(v);

                v.position = vec3(
                        r * sin(alpha),
                        (j * 2 - 1) * height / 2,
                        r * cos(alpha));
                v.UV = vec2(0.5f+sin(alpha)*r/R, 0.5f+cos(alpha)*r/R);
                vertices.push_back(v);
            }
        }

        Triangle t;
        for (unsigned int i = 0; i < density; ++i) {

            t.indices.a = offset + i * 2;
            t.indices.b = offset + (i + 1) * 2;
            t.indices.c = offset + i * 2 + 1;
            triangles.push_back(t);

            t.indices.a = offset + (i + 1) * 2;
            t.indices.b = offset + (i + 1) * 2 + 1;
            t.indices.c = offset + i * 2 + 1;
            triangles.push_back(t);
        }
        offset += 2 * (density + 1);

        for (unsigned int i = 0; i < density; ++i) {

            t.indices.c = offset + i * 2;
            t.indices.b = offset + (i + 1) * 2;
            t.indices.a = offset + i * 2 + 1;
            triangles.push_back(t);

            t.indices.c = offset + (i + 1) * 2;
            t.indices.b = offset + (i + 1) * 2 + 1;
            t.indices.a = offset + i * 2 + 1;
            triangles.push_back(t);
        }
        offset += 2 * (density + 1);

        for (unsigned int i = 0; i < density; ++i) {
            t.indices.c = offset + i * 2;
            t.indices.b = offset + (i + 1) * 2;
            t.indices.a = offset + i * 2 + 1;
            triangles.push_back(t);

            t.indices.c = offset + (i + 1) * 2;
            t.indices.b = offset + (i + 1) * 2 + 1;
            t.indices.a = offset + i * 2 + 1;
            triangles.push_back(t);
        }
        offset += 2 * (density +1);

        for (unsigned int i = 0; i < density; ++i) {
            t.indices.a = offset + i * 2;
            t.indices.b = offset + (i + 1) * 2;
            t.indices.c = offset + i * 2 + 1;
            triangles.push_back(t);

            t.indices.a = offset + (i + 1) * 2;
            t.indices.b = offset + (i + 1) * 2 + 1;
            t.indices.c = offset + i * 2 + 1;
            triangles.push_back(t);
        }
    }
}
