//
//  CubeLinesMesh.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __CubeLinesMesh_H_
#define __CubeLinesMesh_H_

#include "../LinesMesh.h"

namespace AKOpenGLEngine {

    class CubeLinesMesh : public LinesMesh {

    public:
        CubeLinesMesh();
    };
}


#endif //__CubeLinesMesh_H_
