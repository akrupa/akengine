//
//  CubeLinesMesh.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <glm/glm.hpp>
#include <iostream>

#include "CubeLinesMesh.h"
#include "../Line.h"

using namespace glm;

namespace AKOpenGLEngine {

    CubeLinesMesh::CubeLinesMesh() : LinesMesh() {
        name = "CubeLinesMesh";
        Vertex v;
        v.normal = vec3(0.0f, 1.0f, 0.0f);
        v.color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
        for (int i = 0; i < 2; ++i) {
            v.position = vec3(-1, i*2-1, -1);
            vertices.push_back(v);
            v.position = vec3(-1, i*2-1, 1);
            vertices.push_back(v);
            v.position = vec3(1, i*2-1, 1);
            vertices.push_back(v);
            v.position = vec3(1, i*2-1, -1);
            vertices.push_back(v);
        }
        Line l;
        for (int i = 0; i < 4; i++) {
            l.indices.a = (unsigned int)i%4;
            l.indices.b = (unsigned int)(i+1)%4;
            lines.push_back(l);
        }

        for (int i = 0; i < 4; ++i) {
            l.indices.a = (unsigned int)4+i%4;
            l.indices.b = (unsigned int)4+(i+1)%4;
            lines.push_back(l);
        }
        for (int i = 0; i < 4; ++i) {
            l.indices.a = (unsigned int)i%4;
            l.indices.b = (unsigned int)4+i%4;
            lines.push_back(l);
        }

    }
}
