//
//  CubeMesh.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 27.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "CubeMesh.h"
#include "../Triangle.h"

using namespace glm;

namespace AKOpenGLEngine {

    CubeMesh::CubeMesh(vec4 const& color) : TrianglesMesh() {
        name = "CubeMesh";
        Vertex v;
        v.color = color;

        v.normal = vec3(0.0f, 1.0f, 0.0f);
        v.tangent = vec3(1.0f, 0.0f, 0.0f);

        v.position = vec3(-1.0f, 1.0f, 1.0f);
        v.UV = vec2(1,0);
        vertices.push_back(v);

        v.position = vec3( 1.0f, 1.0f, 1.0f);
        v.UV = vec2(0,0);
        vertices.push_back(v);

        v.position = vec3( 1.0f, 1.0f, -1.0f);
        v.UV = vec2(0,1);
        vertices.push_back(v);

        v.position = vec3(-1.0f, 1.0f, -1.0f);
        v.UV = vec2(1,1);
        vertices.push_back(v);

        v.normal = vec3(0.0f, -1.0f, 0.0f);
        v.tangent = vec3(1.0f, 0.0f, 0.0f);

        v.position = vec3(-1.0f, -1.0f, 1.0f);
        v.UV = vec2(0,1);
        vertices.push_back(v);

        v.position = vec3( 1.0f, -1.0f, 1.0f);
        v.UV = vec2(1,1);
        vertices.push_back(v);

        v.position = vec3( 1.0f, -1.0f, -1.0f);
        v.UV = vec2(1,0);
        vertices.push_back(v);

        v.position = vec3(-1.0f, -1.0f, -1.0f);
        v.UV = vec2(0,0);
        vertices.push_back(v);

        v.normal = vec3(1.0f, 0.0f, 0.0f);
        v.tangent = vec3(0.0f, 0.0f, 1.0f);

        v.position = vec3( 1.0f, -1.0f, 1.0f);
        v.UV = vec2(0,0);
        vertices.push_back(v);

        v.position = vec3( 1.0f, 1.0f, 1.0f);
        v.UV = vec2(0,1);
        vertices.push_back(v);

        v.position = vec3( 1.0f, 1.0f, -1.0f);
        v.UV = vec2(1,1);
        vertices.push_back(v);

        v.position = vec3( 1.0f, -1.0f, -1.0f);
        v.UV = vec2(1,0);
        vertices.push_back(v);

        v.normal = vec3(-1.0f, 0.0f, 0.0f);
        v.tangent = vec3(0.0f, 0.0f, 1.0f);

        v.position = vec3(-1.0f, -1.0f, 1.0f);
        v.UV = vec2(0,0);
        vertices.push_back(v);

        v.position = vec3(-1.0f, 1.0f, 1.0f);
        v.UV = vec2(0,1);
        vertices.push_back(v);

        v.position = vec3(-1.0f, 1.0f, -1.0f);
        v.UV = vec2(1,1);
        vertices.push_back(v);

        v.position = vec3(-1.0f, -1.0f, -1.0f);
        v.UV = vec2(1,0);
        vertices.push_back(v);

        v.normal = vec3(0.0f, 0.0f, 1.0f);
        v.tangent = vec3(0.0f, 1.0f, 0.0f);

        v.position = vec3(-1.0f, 1.0f, 1.0f);
        v.UV = vec2(0,1);
        vertices.push_back(v);

        v.position = vec3( 1.0f, 1.0f, 1.0f);
        v.UV = vec2(1,1);
        vertices.push_back(v);

        v.position = vec3( 1.0f, -1.0f, 1.0f);
        v.UV = vec2(1,0);
        vertices.push_back(v);

        v.position = vec3(-1.0f, -1.0f, 1.0f);
        v.UV = vec2(0,0);
        vertices.push_back(v);

        v.normal = vec3(0.0f, 0.0f,-1.0f);
        v.tangent = vec3(0.0f, 1.0f, -0.0f);

        v.position = vec3(-1.0f, 1.0f,-1.0f);
        v.UV = vec2(1,1);
        vertices.push_back(v);

        v.position = vec3( 1.0f, 1.0f,-1.0f);
        v.UV = vec2(0,1);
        vertices.push_back(v);

        v.position = vec3( 1.0f, -1.0f,-1.0f);
        v.UV = vec2(0,0);
        vertices.push_back(v);

        v.position = vec3(-1.0f, -1.0f,-1.0f);
        v.UV = vec2(1,0);
        vertices.push_back(v);


        Triangle t;

        t.indices.a = 0;
        t.indices.b = 1;
        t.indices.c = 2;
        triangles.push_back(t);

        t.indices.a = 0;
        t.indices.b = 2;
        t.indices.c = 3;
        triangles.push_back(t);

        t.indices.a = 6;
        t.indices.b = 5;
        t.indices.c = 4;
        triangles.push_back(t);

        t.indices.a = 7;
        t.indices.b = 6;
        t.indices.c = 4;
        triangles.push_back(t);

        t.indices.a = 10;
        t.indices.b = 9;
        t.indices.c = 8;
        triangles.push_back(t);

        t.indices.a = 11;
        t.indices.b = 10;
        t.indices.c = 8;
        triangles.push_back(t);

        t.indices.a = 12;
        t.indices.b = 13;
        t.indices.c = 14;
        triangles.push_back(t);

        t.indices.a = 12;
        t.indices.b = 14;
        t.indices.c = 15;
        triangles.push_back(t);

        t.indices.a = 18;
        t.indices.b = 17;
        t.indices.c = 16;
        triangles.push_back(t);

        t.indices.a = 19;
        t.indices.b = 18;
        t.indices.c = 16;
        triangles.push_back(t);

        t.indices.a = 20;
        t.indices.b = 21;
        t.indices.c = 22;
        triangles.push_back(t);

        t.indices.a = 20;
        t.indices.b = 22;
        t.indices.c = 23;
        triangles.push_back(t);

    }

    CubeMesh::CubeMesh(std::map<std::string, float> const& parameters) {
    	vec4 color;
    	color.x = parameters.find("colorX")->second;
    	color.y = parameters.find("colorY")->second;
    	color.z = parameters.find("colorZ")->second;
    	color.w = parameters.find("colorW")->second;

    	new(this) CubeMesh(color);
    }
}
