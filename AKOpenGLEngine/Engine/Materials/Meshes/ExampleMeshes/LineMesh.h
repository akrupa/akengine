//
//  LineMesh.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __LineMesh_H_
#define __LineMesh_H_

#include "../LinesMesh.h"

namespace AKOpenGLEngine {

    class LineMesh : public LinesMesh {
    public:
        LineMesh();
    };
}


#endif //__LineMesh_H_
