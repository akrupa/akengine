//
//  ConeMesh.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "ConeMesh.h"
#include "../Triangle.h"

using namespace glm;

namespace AKOpenGLEngine {
    ConeMesh::ConeMesh(float R, float height, float chop, unsigned int density, glm::vec4 const& color) {
        name = "ConeMesh";
        Vertex v;
        v.color = color;

        float chopHeight = chop * height;
        float chopR = (1 - chop) * R;

        for (unsigned int i = 0; i <= density; ++i) {
            float alpha = i * (float) M_PI * 2 / density;

            v.normal = normalize(vec3(
                    sin(alpha),
                    R/height,
                    cos(alpha)));

            v.position = vec3(R * sin(alpha),
                    -chopHeight / 2,
                    R * cos(alpha));
            v.UV = vec2((float) i / (float) density, 0);
            vertices.push_back(v);

            v.position = vec3(chopR * sin(alpha),
                    chopHeight / 2,
                    chopR * cos(alpha));
            v.UV = vec2((float) i / (float) density, 1);
            vertices.push_back(v);
        }

        unsigned int looper = chop==1.0f?1:2;

        for (unsigned int j = 0; j < looper; j++) {
            v.position = vec3(0, (j * 2 - 1) * chopHeight / 2, 0);
            v.normal = vec3(0, (j * 2 - 1), 0);
            v.UV = vec2(0, 0);
            vertices.push_back(v);
            for (unsigned int i = 0; i <= density; ++i) {
                float alpha = i * (float) M_PI * 2 / density;
                v.position = vec3(
                        (j==1?chopR:R) * sin(alpha),
                        (j * 2 - 1) * chopHeight / 2,
                        (j==1?chopR:R) * cos(alpha));
                v.UV = vec2(sin(alpha), cos(alpha));
                vertices.push_back(v);
            }
        }

        Triangle t;
        for (unsigned int i = 0; i < density; ++i) {

            t.indices.a = i * 2;
            t.indices.b = (i + 1) * 2;
            t.indices.c = i * 2 + 1;
            triangles.push_back(t);

            if (chop != 1.0f) {
                t.indices.a = (i + 1) * 2;
                t.indices.b = (i + 1) * 2 + 1;
                t.indices.c = i * 2 + 1;
                triangles.push_back(t);
            }
        }
        for (unsigned int j = 0; j < looper; j++) {
            unsigned int offset =  2*(density+1) + j*(density+2);
            for (unsigned int i = 0; i <= density; ++i) {
                t.indices.a = offset;
                t.indices.b = offset +i%(density*2);
                t.indices.c = offset+(i+1)%(density*2);
                if(j==0) {
                    unsigned int temp = t.indices.a;
                    t.indices.a = t.indices.c;
                    t.indices.c = temp;
                }
                triangles.push_back(t);
            }
        }
    }
}
