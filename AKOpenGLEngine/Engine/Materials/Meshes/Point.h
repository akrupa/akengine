//
//  Point.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Point_H_
#define __Point_H_

namespace AKOpenGLEngine {

    struct Point {
        union indices {
            struct
            {
                unsigned int a;
            };
            unsigned int v[1];
        } indices;

    };
}


#endif //__Point_H_
