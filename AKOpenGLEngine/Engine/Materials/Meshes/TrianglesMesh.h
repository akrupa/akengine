//
//  TrianglesMesh.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 27.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __TrianglesMesh_H_
#define __TrianglesMesh_H_

#include "Mesh.h"

namespace AKOpenGLEngine {

    struct Triangle;

    class TrianglesMesh : public Mesh {

    protected:
        std::vector<Triangle>triangles;
        TrianglesMesh();

    public:
        TrianglesMesh(std::vector<Vertex>points, bool stripTriangle = true);
        TrianglesMesh(std::vector<Vertex>points,  std::vector<Triangle> triangles);
        virtual unsigned int *GetIndexData() override;
        virtual unsigned int GetIndexDataSize() override;

        virtual Vertex *GetVertexData() override;
        virtual unsigned int GetVertexDataSize() override;

        virtual unsigned int GetIndexDrawSize() override;

        virtual GLenum GetDrawType() override;
    };
}


#endif //__TrianglesMesh_H_
