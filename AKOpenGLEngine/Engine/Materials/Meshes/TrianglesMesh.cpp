//
//  TrianglesMesh.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 27.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "TrianglesMesh.h"
#include "Triangle.h"

using namespace std;

namespace AKOpenGLEngine {

    TrianglesMesh::TrianglesMesh() : Mesh() {
        triangles = vector<Triangle>();
    }

    TrianglesMesh::TrianglesMesh(vector<Vertex> points, bool stripTriangle) {
        this->triangles = vector<Triangle>(stripTriangle?points.size()-2:points.size()/3);
        Triangle t;

        if(stripTriangle) {
            for(size_t i=0; i < points.size()-2; ++i) {
                t.indices.a = (unsigned int)i;
                t.indices.b =  (unsigned int)i+1;
                t.indices.c =  (unsigned int)i+2;
                this->triangles.push_back(t);
            }
        } else {
            for(size_t i=0; i < points.size()/3; ++i) {
                t.indices.a = (unsigned int)i*3;
                t.indices.b =  (unsigned int)i*3+1;
                t.indices.b =  (unsigned int)i*3+2;
                this->triangles.push_back(t);
            }
        }

        this->vertices = points;
    }

    TrianglesMesh::TrianglesMesh(std::vector<Vertex> points, vector<Triangle> triangles) {
        this->vertices = points;
        this->triangles = triangles;
        meshChangeCounter++;
    }

    unsigned int*TrianglesMesh::GetIndexData() {
        return (unsigned int*)triangles.data();
    }

    unsigned int TrianglesMesh::GetIndexDataSize() {
        return (unsigned int)(triangles.size() * sizeof(Triangle));
    }

    unsigned int TrianglesMesh::GetIndexDrawSize() {
        return (unsigned int)triangles.size()*3;
    }

    Vertex*TrianglesMesh::GetVertexData() {
        return vertices.data();
    }

    unsigned int TrianglesMesh::GetVertexDataSize() {
        return (unsigned int)(vertices.size() * sizeof(Vertex));
    }

    GLenum TrianglesMesh::GetDrawType() {
        return GL_TRIANGLES;
    }
}
