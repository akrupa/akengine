//
//  Texture.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 07.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <yaml-cpp/yaml.h>

#include "Texture.h"

namespace AKOpenGLEngine {

    std::string Texture::GetName() const {
        return name;
    }

    int Texture::GetBind() const {
        return bind;
    }

    GLenum Texture::GetImageFormat() const {
        return image_format;
    }

    GLenum Texture::GetInternalFormat() const {
        return internal_format;
    }

    Texture::Texture(int bind, std::string name, GLenum image_format, GLenum internal_format, GLint level, GLint border) {
        this->bind = bind;
        this->name = name;
        this->image_format = image_format;
        this->internal_format = internal_format;
        this->level = level;
        this->border = border;
    }

    YAML::Emitter &operator<<(YAML::Emitter &out, const Texture *t) {
        out << YAML::BeginMap;
        out << YAML::Key << "id" << YAML::Value << (long long int) t;
        out << YAML::Key << "name" << YAML::Value << t->name;
        out << YAML::Key << "image_format" << YAML::Value << t->image_format;
        out << YAML::Key << "internal_format" << YAML::Value << t->internal_format;
        out << YAML::Key << "level" << YAML::Value << t->level;
        out << YAML::Key << "border" << YAML::Value << t->border;
        out << YAML::EndMap;
        return out;
    }
}
