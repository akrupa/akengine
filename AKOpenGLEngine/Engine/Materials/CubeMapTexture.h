//
//  CubeMapTexture.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 11.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __AKOpenGLEngine__CubeMapTexture__
#define __AKOpenGLEngine__CubeMapTexture__

#include "Texture.h"

namespace AKOpenGLEngine {
    
    class CubeMapTexture : public Texture {
        
    public:
        CubeMapTexture(int bind, std::string name, GLenum image_format);
    };
}

#endif /* defined(__AKOpenGLEngine__CubeMapTexture__) */
