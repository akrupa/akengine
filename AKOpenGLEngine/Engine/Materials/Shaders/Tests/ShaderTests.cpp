//
//  ShaderTests.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 09.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "ShaderTests.h"
#include "../Shaders.h"

namespace AKOpenGLEngine {

    TEST_F(ShaderTests, AmbientShader) {
        shader = Shaders::getInstance().GetAmbientShader();
        EXPECT_NE(nullptr, shader);
        EXPECT_EQ(true, shader->isCompiled());
        EXPECT_EQ(shader, Shaders::getInstance().GetShader("AmbientShader"));
        EXPECT_NE(shader, Shaders::getInstance().GetShader("RandomShader"));
        for(auto &uniform : shader->getUniforms()) {
            EXPECT_NE(-1, shader->getUniformLocation(uniform));
        }
        EXPECT_EQ(-1, shader->getUniformLocation("RandomUniform"));
    }

    TEST_F(ShaderTests, AmbientTexturedShader) {
        shader = Shaders::getInstance().GetAmbientTexturedShader();
        EXPECT_NE(nullptr, shader);
        EXPECT_EQ(true, shader->isCompiled());
        EXPECT_EQ(shader, Shaders::getInstance().GetShader("AmbientTexturedShader"));
        EXPECT_NE(shader, Shaders::getInstance().GetShader("RandomShader"));
        for(auto &uniform : shader->getUniforms()) {
            EXPECT_NE(-1, shader->getUniformLocation(uniform));
        }
        EXPECT_EQ(-1, shader->getUniformLocation("RandomUniform"));
    }

    TEST_F(ShaderTests, DebugNormalsShader) {
        shader = Shaders::getInstance().GetDebugNormalsShader();
        EXPECT_NE(nullptr, shader);
        EXPECT_EQ(true, shader->isCompiled());
        EXPECT_EQ(shader, Shaders::getInstance().GetShader("DebugNormalsShader"));
        EXPECT_NE(shader, Shaders::getInstance().GetShader("RandomShader"));
        for(auto &uniform : shader->getUniforms()) {
            EXPECT_NE(-1, shader->getUniformLocation(uniform));
        }
        EXPECT_EQ(-1, shader->getUniformLocation("RandomUniform"));
    }

    TEST_F(ShaderTests, UnlitBlendedTexturedShader) {
        shader = Shaders::getInstance().GetUnlitBlendedTexturedShader();
        EXPECT_NE(nullptr, shader);
        EXPECT_EQ(true, shader->isCompiled());
        EXPECT_EQ(shader, Shaders::getInstance().GetShader("UnlitBlendedTexturedShader"));
        EXPECT_NE(shader, Shaders::getInstance().GetShader("RandomShader"));
        for(auto &uniform : shader->getUniforms()) {
            EXPECT_NE(-1, shader->getUniformLocation(uniform));
        }
        EXPECT_EQ(-1, shader->getUniformLocation("RandomUniform"));
    }

    TEST_F(ShaderTests, UnlitShader) {
        shader = Shaders::getInstance().GetUnlitShader();
        EXPECT_NE(nullptr, shader);
        EXPECT_EQ(true, shader->isCompiled());
        EXPECT_EQ(shader, Shaders::getInstance().GetShader("UnlitShader"));
        EXPECT_NE(shader, Shaders::getInstance().GetShader("RandomShader"));
        for(auto &uniform : shader->getUniforms()) {
            EXPECT_NE(-1, shader->getUniformLocation(uniform));
        }
        EXPECT_EQ(-1, shader->getUniformLocation("RandomUniform"));
    }

    TEST_F(ShaderTests, UnlitTexturedShader) {
        shader = Shaders::getInstance().GetUnlitTexturedShader();
        EXPECT_NE(nullptr, shader);
        EXPECT_EQ(true, shader->isCompiled());
        EXPECT_EQ(shader, Shaders::getInstance().GetShader("UnlitTexturedShader"));
        EXPECT_NE(shader, Shaders::getInstance().GetShader("RandomShader"));
        for(auto &uniform : shader->getUniforms()) {
            EXPECT_NE(-1, shader->getUniformLocation(uniform));
        }
        EXPECT_EQ(-1, shader->getUniformLocation("RandomUniform"));
    }

    TEST_F(ShaderTests, DiffuseShader) {
        shader = Shaders::getInstance().GetDiffuseShader();
        EXPECT_NE(nullptr, shader);
        EXPECT_EQ(true, shader->isCompiled());
        EXPECT_EQ(shader, Shaders::getInstance().GetShader("DiffuseShader"));
        EXPECT_NE(shader, Shaders::getInstance().GetShader("RandomShader"));
        for(auto &uniform : shader->getUniforms()) {
            EXPECT_NE(-1, shader->getUniformLocation(uniform));
        }
        EXPECT_EQ(-1, shader->getUniformLocation("RandomUniform"));
    }

    TEST_F(ShaderTests, DiffuseTexturedShader) {
        shader = Shaders::getInstance().GetDiffuseTexturedShader();
        EXPECT_NE(nullptr, shader);
        EXPECT_EQ(true, shader->isCompiled());
        EXPECT_EQ(shader, Shaders::getInstance().GetShader("DiffuseTexturedShader"));
        EXPECT_NE(shader, Shaders::getInstance().GetShader("RandomShader"));
        for(auto &uniform : shader->getUniforms()) {
            EXPECT_NE(-1, shader->getUniformLocation(uniform));
        }
        EXPECT_EQ(-1, shader->getUniformLocation("RandomUniform"));
    }

    TEST_F(ShaderTests, DiffuseSpecularShader) {
        shader = Shaders::getInstance().GetDiffuseSpecularShader();
        EXPECT_NE(nullptr, shader);
        EXPECT_EQ(true, shader->isCompiled());
        EXPECT_EQ(shader, Shaders::getInstance().GetShader("DiffuseSpecularShader"));
        EXPECT_NE(shader, Shaders::getInstance().GetShader("RandomShader"));
        for(auto &uniform : shader->getUniforms()) {
            EXPECT_NE(-1, shader->getUniformLocation(uniform));
        }
        EXPECT_EQ(-1, shader->getUniformLocation("RandomUniform"));
    }

    void ShaderTests::TearDown() {
        RendererTests::TearDown();
        Shaders::getInstance().ReleaseShaders();
    }
}