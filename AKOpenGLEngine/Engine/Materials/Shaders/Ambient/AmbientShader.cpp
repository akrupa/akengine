//
//  AmbientShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "AmbientShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../VertexAttrib.h"

using namespace std;

namespace AKOpenGLEngine {
    string AmbientShader::getShaderName() const {
        return "AmbientShader";
    }

    void AmbientShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribPosition, "in_Position"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribColor, "in_Color"));
    }

    void AmbientShader::fetchUniformLocations() {
        uniforms.push_back("MATRIX_MVP");
        uniforms.push_back("Color");
        SurfaceShader::fetchUniformLocations();
    }

    bool AmbientShader::isUsingLight() {
        return false;
    }
}