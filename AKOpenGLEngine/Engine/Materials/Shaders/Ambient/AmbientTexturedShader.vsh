#version 410 core

uniform mat4 MATRIX_MVP;

in vec3 in_Position;
in vec4 in_Color;
in vec2 in_UV;

out vec4 pass_Color;
out vec2 pass_UV;

void main(void) {
    pass_Color = in_Color;
    pass_UV = in_UV;
    gl_Position = MATRIX_MVP * vec4(in_Position, 1.0);
}