#version 410 core

uniform vec4 Color;

in vec4 pass_Color;
in vec2 pass_UV;

out vec4 out_Color;

uniform sampler2D texture0;

void main(void) {
    vec4 scatteredLight = Color;
	out_Color = min(pass_Color*scatteredLight, 1.0) * texture( texture0, pass_UV );
}