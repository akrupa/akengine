#version 410 core

uniform vec4 Color;

in vec4 pass_Color;

out vec4 out_Color;

void main(void) {
    vec4 scatteredLight = Color;
    out_Color = min(pass_Color*scatteredLight, 1.0);
}