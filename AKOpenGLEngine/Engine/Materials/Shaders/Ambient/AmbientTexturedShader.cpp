//
//  AmbientTexturedShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 18.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "AmbientTexturedShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../VertexAttrib.h"

using namespace std;

namespace AKOpenGLEngine {
    string AmbientTexturedShader::getShaderName() const {
        return "AmbientTexturedShader";
    }

    void AmbientTexturedShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribPosition, "in_Position"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribUV, "in_UV"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribColor, "in_Color"));
    }

    void AmbientTexturedShader::fetchUniformLocations() {
        uniforms.push_back("MATRIX_MVP");
        uniforms.push_back("Color");
        uniforms.push_back("texture0");
        SurfaceShader::fetchUniformLocations();
    }

    bool AmbientTexturedShader::isUsingLight() {
        return false;
    }
}