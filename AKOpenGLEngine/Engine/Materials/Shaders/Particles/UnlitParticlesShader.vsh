#version 410 core

uniform mat4 VPMatrix;
uniform vec4 CameraRightWorldSpace;
uniform vec4 CameraUpWorldspace;

in vec3 in_Position;
in vec3 in_Vertex;

out vec4 pass_Color;

void main(void) {
    pass_Color = vec4(0.2,0,0.2,1);
    vec3 vertexPos = in_Position + CameraRightWorldSpace.xyz * in_Vertex.x + CameraUpWorldspace.xyz * in_Vertex.y;
	gl_Position = VPMatrix * vec4(vertexPos, 1.0);
}