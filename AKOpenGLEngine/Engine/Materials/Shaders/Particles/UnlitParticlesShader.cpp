//
//  UnlitParticlesShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 20.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "UnlitParticlesShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../ParticleAttrib.h"

using namespace std;

namespace AKOpenGLEngine {

    string UnlitParticlesShader::getShaderName() const {
        return "UnlitParticlesShader";
    }

    void UnlitParticlesShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, ParticleAttribPosition, "in_Position"));
        GL_CHECK(glBindAttribLocation(m_program, ParticleAttribVertex, "in_Vertex"));
    }

    void UnlitParticlesShader::fetchUniformLocations() {
        uniforms.push_back("VPMatrix");
        uniforms.push_back("CameraRightWorldSpace");
        uniforms.push_back("CameraUpWorldspace");
        SurfaceShader::fetchUniformLocations();
    }

    bool UnlitParticlesShader::isUsingLight() {
        return false;
    }
}