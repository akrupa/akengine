//
//  SchwarzschildBlackHoleSkyBox.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 13.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __AKOpenGLEngine__SchwarzschildBlackHoleSkyBox__
#define __AKOpenGLEngine__SchwarzschildBlackHoleSkyBox__

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class SchwarzschildBlackHoleSkyBox : public SurfaceShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}

#endif /* defined(__AKOpenGLEngine__SchwarzschildBlackHoleSkyBox__) */
