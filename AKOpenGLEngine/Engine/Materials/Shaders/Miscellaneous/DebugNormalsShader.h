//
//  DebugNormalsShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __DebugNormalsShader_H_
#define __DebugNormalsShader_H_

#include "../GeometryShader.h"

namespace AKOpenGLEngine {
    class DebugNormalsShader : public GeometryShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}


#endif //__DebugNormalsShader_H_
