#version 410 core

//
//  SchwarzschildBlackHoleSkyBox.vsh
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 13.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

uniform mat4 ViewMatrix;

in vec3 in_Position;

out vec3 pass_UV;

void main(void) {
    pass_UV = in_Position;
    gl_Position = ViewMatrix * vec4(in_Position, 1.0);
}

