//
//  PostProcessingShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 02.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __AKOpenGLEngine__PostProcessingShader__
#define __AKOpenGLEngine__PostProcessingShader__

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {
    
    class PostProcessingShader : public SurfaceShader {
        
    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}

#endif /* defined(__AKOpenGLEngine__PostProcessingShader__) */
