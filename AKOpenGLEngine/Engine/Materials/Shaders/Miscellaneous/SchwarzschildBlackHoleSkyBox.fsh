#version 410 core

//
//  SchwarzschildBlackHoleSkyBox.fsh
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 13.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#define M_PI 3.14159265358

uniform samplerCube texture0;
uniform vec4 cameraPosition;

in vec3 pass_UV;

out vec4 out_Color;

vec3 blackHolePosition = vec3(0,0,-1000);

float M = 20.0; // times Solar Mass
float Gc2 = 2.95; // simplified constant

void main(void) {
    
    float rs = Gc2*M;
    vec3 direction = normalize(pass_UV);
    
    float dott = dot(direction, cameraPosition.xyz-blackHolePosition);
    
    float d = length(cross(cameraPosition.xyz-blackHolePosition, cameraPosition.xyz+direction-cameraPosition.xyz))/(length(direction)*length(direction));
    float dist = distance(cameraPosition.xyz, blackHolePosition);
    
    if (d <= rs && dott<0.0) {
        out_Color = vec4(0,0,0,1);
    } else {
        if (dott>=0.0 && dist>rs) {
            d = dist;
        }
        float angle = 2.0*rs/d;
        vec3 point = cameraPosition.xyz + direction * sqrt(d*d + dist*dist);
        vec3 u = normalize(cross(blackHolePosition-point, direction));
        
        mat3 uu = mat3(u.x*u.x,     u.x*u.y,    u.x*u.z,
                       u.x*u.y,     u.y*u.y,    u.y*u.z,
                       u.x*u.z,     u.y*u.z,    u.z*u.z);
        mat3 ux = mat3(0,       -u.z,   u.y,
                       u.z,     0,      -u.x,
                       -u.y,    u.x,    0);
        mat3 I = mat3(1,0,0,
                      0,1,0,
                      0,0,1);
        mat3 R = cos(angle)*I+sin(angle)*ux+(1.0-cos(angle))*uu;
        
        out_Color = texture(texture0, R*pass_UV);
    }
}