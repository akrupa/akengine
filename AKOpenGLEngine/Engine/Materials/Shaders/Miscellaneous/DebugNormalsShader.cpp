//
//  DebugNormalsShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "DebugNormalsShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../VertexAttrib.h"

using namespace std;

namespace AKOpenGLEngine {

    string DebugNormalsShader::getShaderName() const {
        return "DebugNormalsShader";
    }

    void DebugNormalsShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribPosition, "in_Position"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribNormal, "in_Normal"));
    }

    void DebugNormalsShader::fetchUniformLocations() {
        uniforms.push_back("MATRIX_MVP");
        GeometryShader::fetchUniformLocations();
    }

    bool DebugNormalsShader::isUsingLight() {
        return false;
    }
}
