//
//  SkyBoxShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 11.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "SkyBoxShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../VertexAttrib.h"

using namespace std;

namespace AKOpenGLEngine {

    string SkyBoxShader::getShaderName() const {
        return "SkyBoxShader";
    }

    void SkyBoxShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribPosition, "in_Position"));
    }

    void SkyBoxShader::fetchUniformLocations() {
        uniforms.push_back("ViewMatrix");
        uniforms.push_back("texture0");
        SurfaceShader::fetchUniformLocations();
    }

    bool SkyBoxShader::isUsingLight() {
        return true;
    }
}
