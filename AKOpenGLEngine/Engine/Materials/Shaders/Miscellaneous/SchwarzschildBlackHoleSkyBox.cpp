//
//  SchwarzschildBlackHoleSkyBox.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 13.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "SchwarzschildBlackHoleSkyBox.h"
#include "../../../Utilities/GLDebug.h"
#include "../VertexAttrib.h"

using namespace std;

namespace AKOpenGLEngine {

    string SchwarzschildBlackHoleSkyBox::getShaderName() const {
        return "SchwarzschildBlackHoleSkyBox";
    }

    void SchwarzschildBlackHoleSkyBox::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribPosition, "in_Position"));
    }

    void SchwarzschildBlackHoleSkyBox::fetchUniformLocations() {
        uniforms.push_back("ViewMatrix");
        uniforms.push_back("texture0");
        uniforms.push_back("cameraPosition");
        SurfaceShader::fetchUniformLocations();
    }

    bool SchwarzschildBlackHoleSkyBox::isUsingLight() {
        return true;
    }
}
