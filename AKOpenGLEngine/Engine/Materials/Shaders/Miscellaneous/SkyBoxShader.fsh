#version 410 core

//
//  SkyBoxShader.fsh
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 11.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

uniform samplerCube texture0;

in vec3 pass_UV;

out vec4 out_Color;

void main(void) {
    out_Color = texture(texture0, pass_UV);
}
