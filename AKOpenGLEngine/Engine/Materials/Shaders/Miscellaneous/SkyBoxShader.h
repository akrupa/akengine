//
//  SkyBoxShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 11.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __AKOpenGLEngine__SkyBoxShader__
#define __AKOpenGLEngine__SkyBoxShader__

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class SkyBoxShader : public SurfaceShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}

#endif /* defined(__AKOpenGLEngine__SkyBoxShader__) */
