#version 410 core

in vec3 in_Position;

out vec2 UV;

void main(){
    gl_Position =  vec4(in_Position,1);
    UV = (in_Position.xy+vec2(1,1))/2.0;
}

