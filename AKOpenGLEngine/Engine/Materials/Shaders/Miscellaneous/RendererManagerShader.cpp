//
//  RendererManagerShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 06.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "RendererManagerShader.h"

namespace AKOpenGLEngine {

    std::string RendererManagerShader::getShaderName() const {
        return "";
    }

    void RendererManagerShader::bindAttributeLocations() {

    }

    void RendererManagerShader::fetchUniformLocations() {
        Shader::fetchUniformLocations();
    }

    bool RendererManagerShader::isUsingLight() {
        return false;
    }
}