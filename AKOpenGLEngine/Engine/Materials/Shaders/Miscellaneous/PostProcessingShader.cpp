//
//  PostProcessingShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 02.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "PostProcessingShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../VertexAttrib.h"

using namespace std;

namespace AKOpenGLEngine {
    
    string PostProcessingShader::getShaderName() const {
        return "PostProcessingShader";
    }
    
    void PostProcessingShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribPosition, "in_Position"));
    }
    
    void PostProcessingShader::fetchUniformLocations() {
        uniforms.push_back("renderedTexture");
        SurfaceShader::fetchUniformLocations();
    }
    
    bool PostProcessingShader::isUsingLight() {
        return false;
    }
}
