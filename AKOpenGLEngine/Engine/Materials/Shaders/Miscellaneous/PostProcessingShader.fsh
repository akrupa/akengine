#version 330 core

in vec2 UV;

out vec3 color;

uniform sampler2D renderedTexture;


float rt_w = 1000.0;
float rt_h = 800.0;

float radius = 200.0;
float angle = 0.8;
vec2 center = vec2(500.0, 400.0);

vec3 PostFX(sampler2D tex, vec2 uv, float time)
{
    vec2 texSize = vec2(rt_w, rt_h);
    vec2 tc = uv * texSize;
    tc -= center;
    float dist = length(tc);
    if (dist < radius)
    {
        float percent = (radius - dist) / radius;
        float theta = percent * percent * angle * 8.0;
        float s = sin(theta);
        float c = cos(theta);
        tc = vec2(dot(tc, vec2(c, -s)), dot(tc, vec2(s, c)));
    }
    tc += center;
    vec3 color = texture(renderedTexture, tc / texSize).rgb;
    return color;
}

void main(){
    color = texture( renderedTexture, UV).xyz ;
    color = PostFX(renderedTexture, UV, 1);
    //color = vec3(1,0,1);
}