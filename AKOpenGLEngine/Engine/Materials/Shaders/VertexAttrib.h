//
//  VertexAttrib.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 15.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
#include <OpenGLES/ES3/glext.h>
#else
#include <GL/glew.h>
#endif

namespace AKOpenGLEngine {

    typedef enum : GLuint {
        VertexAttribPosition,
        VertexAttribNormal,
        VertexAttribUV,
        VertexAttribTangent,
        VertexAttribColor
    } VertexAttrib;

}