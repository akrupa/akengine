//
//  SurfaceShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <string>

#include "SurfaceShader.h"
#include "../../Utilities/GLDebug.h"
#include "../../Utilities/WorkingDirectory.h"

using namespace std;

namespace AKOpenGLEngine {

    void SurfaceShader::setup() {
        GLuint vertShader, fragShader;
        string vertShaderPathname, fragShaderPathname;

        GL_CHECK_RETURN(m_program = glCreateProgram());
        
#if defined(_TEST)
        string shadersDirectory = "/tempShadersTestDirectory/";
#else
        string shadersDirectory = "/shaders/";
#endif

        vertShaderPathname = WorkingDirectory::GetExecutableDirectory()
                + shadersDirectory + getShaderName() + ".vsh";
        if (!compileShader(&vertShader, GL_VERTEX_SHADER, vertShaderPathname)) {
#if !defined(_TEST)
            LOG(ERROR) << "Failed to compile vertex shader: " + getShaderName();
#endif
            return;
        }

        fragShaderPathname = WorkingDirectory::GetExecutableDirectory()
                + shadersDirectory + getShaderName() + ".fsh";
        if (!compileShader(&fragShader, GL_FRAGMENT_SHADER, fragShaderPathname)) {
#if !defined(_TEST)
            LOG(ERROR) << "Failed to compile fragment shader: " + getShaderName();
#endif
            return;
        }
        attachShaders(vertShader, fragShader);
    }

    void SurfaceShader::setup(std::string vertexShaderString, std::string fragmentShaderString) {
        GLuint vertShader, fragShader;
        m_program=2;
        glCreateProgram();
        GL_CHECK_RETURN(m_program = glCreateProgram());
        if (!compileShader(&vertShader, GL_VERTEX_SHADER, vertexShaderString, "stringShaderVertex")) {
#if !defined(_TEST)
            LOG(ERROR) << "Failed to compile vertex shader: " + getShaderName();
#endif
            return;
        }
        if (!compileShader(&fragShader, GL_FRAGMENT_SHADER, fragmentShaderString, "stringShaderFragment")) {
#if !defined(_TEST)
            LOG(ERROR) << "Failed to compile fragment shader: " + getShaderName();
#endif
            return;
        }
        attachShaders(vertShader, fragShader);
    }
}
