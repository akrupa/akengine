#version 410 core

in vec3 pass_Normal;
in vec4 pass_Color;
in vec4 pass_Position;
in vec2 pass_UV;

layout(location = 0) out vec4 out_Color;

uniform sampler2D texture0;

/*
enum LightType : int {
    Directional,
    Point,
    Spot
};
*/

struct Light {
    vec4 typeRangeSpotAngleIntensity;
    vec4 directionOrPosition;
    vec4 color;
};

layout (shared) uniform Lights {
    int numberOfLights;
    Light light[4];
};

void computeLight(int lightNumber, vec3 normal, vec3 v, out float diffuseFactor, out float specularFactor) {
    vec3 s = vec3(0);
    diffuseFactor = 0;
    specularFactor = 0;
    if(light[lightNumber].typeRangeSpotAngleIntensity.x == 0) { //DirectionalLight
        s = light[lightNumber].direction.xyz;
    } else { //Point or spot light
        s = normalize(light[lightNumber].position.xyz-pass_Position.xyz);
    }

    if(light[lightNumber].typeRangeSpotAngleIntensity.x == 2) { //SpotLight
        float angle = acos(dot(s, light[lightNumber].direction.xyz));
        if (angle < light[lightNumber].typeRangeSpotAngleIntensity.z) {
            diffuseFactor = dot(normal, s);
            vec3 h = normalize (v + s);
            specularFactor = pow(max(dot(h,normal), 0.0), 10);
        }
    } else {
        diffuseFactor = dot(normal, s);
        vec3 h = normalize (v + s);
        specularFactor = pow(max(dot(h,normal), 0.0), 10);
    }
}

void main() {
	vec4 DiffuseColor = vec4(0);
	vec4 SpecularColor = vec4(0);
	vec3 normal = normalize(pass_Normal);
    vec3 v = normalize(-pass_Position.xyz);

	for(int i = 0; i < numberOfLights; i++) {
	    float diffuseFactor = 0;
	    float specularFactor = 0;
	    computeLight(i, normal, v, diffuseFactor, specularFactor);
	    float lightIntensity = light[i].typeRangeSpotAngleIntensity.w;
        if (diffuseFactor > 0) {
            DiffuseColor += light[i].color * lightIntensity * diffuseFactor;
            SpecularColor += light[i].color * lightIntensity * specularFactor * diffuseFactor;
        }
    }
	out_Color = min(pass_Color*DiffuseColor* texture( texture0, pass_UV ) + SpecularColor, 1.0);
}