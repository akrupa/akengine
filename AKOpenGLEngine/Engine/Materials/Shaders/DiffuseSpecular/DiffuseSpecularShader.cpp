//
//  DiffuseSpecularShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 09.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "DiffuseSpecularShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../VertexAttrib.h"

using namespace std;

namespace AKOpenGLEngine {

    string DiffuseSpecularShader::getShaderName() const {
        return "DiffuseSpecularShader";
    }

    void DiffuseSpecularShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribPosition, "in_Position"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribNormal, "in_Normal"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribColor, "in_Color"));
    }

    void DiffuseSpecularShader::fetchUniformLocations() {
        uniforms.push_back("MATRIX_MVP");
        uniforms.push_back("NormalMatrix");
        uniforms.push_back("MATRIX_MV");
        uniforms.push_back("Color");
        uniformsBlocks.push_back("Lights");
        SurfaceShader::fetchUniformLocations();
    }

    bool DiffuseSpecularShader::isUsingLight() {
        return true;
    }

}