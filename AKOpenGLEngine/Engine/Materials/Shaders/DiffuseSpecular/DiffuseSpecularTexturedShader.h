//
//  DiffuseSpecularTexturedShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 18.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __DiffuseSpecularTexturedShader_H_
#define __DiffuseSpecularTexturedShader_H_

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class DiffuseSpecularTexturedShader : public SurfaceShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}


#endif //__DiffuseSpecularTexturedShader_H_
