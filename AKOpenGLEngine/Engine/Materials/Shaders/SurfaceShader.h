//
//  SurfaceShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __SurfaceShader_H_
#define __SurfaceShader_H_

#include "Shader.h"

namespace AKOpenGLEngine {

    class SurfaceShader : public Shader {

        virtual std::string getShaderName() const = 0;

        virtual void bindAttributeLocations() = 0;

    public:
        virtual void setup() override;
        virtual void setup(std::string vertexShaderString, std::string fragmentShaderString);
    };
}


#endif //__SurfaceShader_H_
