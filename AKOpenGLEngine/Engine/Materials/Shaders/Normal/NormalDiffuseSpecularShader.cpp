//
//  NormalDiffuseSpecularShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 22.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "NormalDiffuseSpecularShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../VertexAttrib.h"

using namespace std;

namespace AKOpenGLEngine {

    string NormalDiffuseSpecularShader::getShaderName() const {
        return "NormalDiffuseSpecularShader";
    }

    void NormalDiffuseSpecularShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribPosition, "in_Position"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribUV, "in_UV"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribTangent, "in_Tangent"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribNormal, "in_Normal"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribColor, "in_Color"));
    }

    void NormalDiffuseSpecularShader::fetchUniformLocations() {
        uniforms.push_back("MATRIX_MVP");
        uniforms.push_back("NormalMatrix");
        uniforms.push_back("Color");
        uniforms.push_back("MATRIX_MV");
        uniformsBlocks.push_back("Lights");
        uniforms.push_back("texture0");
        uniforms.push_back("textureNormal");
        SurfaceShader::fetchUniformLocations();
    }

    bool NormalDiffuseSpecularShader::isUsingLight() {
        return true;
    }
}