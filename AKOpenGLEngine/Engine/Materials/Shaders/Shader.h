//
//  Shader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Shader_H_
#define __Shader_H_

#include <string>
#include <map>
#include <vector>

#include "Utilities/OpenGL.h"

/**
 *  Built-in shader variables:
 *  MATRIX_MVP Model View Projection matrix
 */

namespace YAML {
    class Emitter;
}

namespace AKOpenGLEngine {

    class Shader {

    protected:
        GLuint m_program;
        std::map<std::string, GLint> uniformsLocations;
        std::vector<std::string> uniforms;
        std::vector<std::string> uniformsBlocks;
        bool compiled = true;

        bool compileShader(GLuint *shader, GLenum type, std::string filePath);
        bool compileShader(GLuint *shader, GLenum type, std::string shaderString, std::string filePath);
        bool linkProgram(GLuint prog);
        void attachShaders(GLuint shader1, GLuint shader2);

        virtual std::string getShaderName() const = 0;
        virtual void bindAttributeLocations() = 0;
        virtual void fetchUniformLocations();
        
        std::string generateShader(const std::string shader, const GLenum type) const;

    public:
        virtual ~Shader();

        virtual void setup() = 0;
        virtual bool isUsingLight() = 0;
        bool isCompiled();
        GLuint getProgram();
        GLint getUniformLocation(std::string const& uniform, bool notifyMissingUniform = true) const;
        std::vector<std::string> getUniforms() const;

        friend YAML::Emitter& operator << (YAML::Emitter& out, const Shader* s);
    };
}


#endif //__Shader_H_
