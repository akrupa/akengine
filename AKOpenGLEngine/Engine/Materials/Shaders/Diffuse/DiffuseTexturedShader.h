//
//  DiffuseTexturedShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 18.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __DiffuseTexturedShader_H_
#define __DiffuseTexturedShader_H_

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class DiffuseTexturedShader : public SurfaceShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}


#endif //__DiffuseTexturedShader_H_
