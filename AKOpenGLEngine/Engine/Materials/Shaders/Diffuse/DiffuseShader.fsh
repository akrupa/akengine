#version 410 core

in vec3 pass_Normal;
in vec4 pass_Color;
in vec4 pass_Position;

layout(location = 0) out vec4 out_Color;

/*
enum LightType : int {
    Directional,
    Point,
    Spot
};
*/

struct Light {
    vec4 typeRangeSpotAngleIntensity;
    vec4 direction;
    vec4 position;
    vec4 color;
};

layout (shared) uniform Lights {
    int numberOfLights;
    Light light[4];
};

void computeLight(int lightNumber, vec3 normal, vec3 v, out float diffuseFactor) {
    vec3 s = vec3(0);
    diffuseFactor = 0;
    if(light[lightNumber].typeRangeSpotAngleIntensity.x == 0.0) { //DirectionalLight
        s = light[lightNumber].direction.xyz;
    } else { //Point or spot light
        s = normalize(light[lightNumber].position.xyz-pass_Position.xyz);
    }

    if(light[lightNumber].typeRangeSpotAngleIntensity.x == 2.0) { //SpotLight
        float angle = acos(dot(s, light[lightNumber].direction.xyz));
        if (angle < light[lightNumber].typeRangeSpotAngleIntensity.z) {
            diffuseFactor = dot(normal, s);
        }
    } else {
        diffuseFactor = dot(normal, s);
    }
}

void main() {
	vec4 DiffuseColor = vec4(0);
	vec3 normal = normalize(pass_Normal);
    vec3 v = normalize(-pass_Position.xyz);

	for(int i = 0; i < numberOfLights; i++) {
	    float diffuseFactor = 0;
	    computeLight(i, normal, v, diffuseFactor);
	    float lightIntensity = light[i].typeRangeSpotAngleIntensity.w;
        if (diffuseFactor > 0) {
            DiffuseColor += light[i].color * lightIntensity * diffuseFactor;
        }
    }
	out_Color = min(pass_Color*DiffuseColor, 1.0);
}