//
//  DiffuseTexturedShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 18.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "DiffuseTexturedShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../VertexAttrib.h"

using namespace std;

namespace AKOpenGLEngine {

    string DiffuseTexturedShader::getShaderName() const {
        return "DiffuseTexturedShader";
    }

    void DiffuseTexturedShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribPosition, "in_Position"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribUV, "in_UV"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribNormal, "in_Normal"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribColor, "in_Color"));
    }

    void DiffuseTexturedShader::fetchUniformLocations() {
        uniforms.push_back("MATRIX_MVP");
        uniforms.push_back("NormalMatrix");
        uniforms.push_back("MATRIX_MV");
        uniforms.push_back("Color");
        uniformsBlocks.push_back("Lights");
        uniforms.push_back("texture0");
        SurfaceShader::fetchUniformLocations();
    }

    bool DiffuseTexturedShader::isUsingLight() {
        return true;
    }
}