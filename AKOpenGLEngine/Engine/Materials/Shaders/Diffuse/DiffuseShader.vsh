#version 410 core

uniform mat4 MATRIX_MVP;
uniform mat4 NormalMatrix;
uniform mat4 MATRIX_MV;

uniform vec4 Color;

in vec3 in_Position;
in vec3 in_Normal;
in vec4 in_Color;

out vec3 pass_Normal;
out vec4 pass_Color;
out vec4 pass_Position;

void main(void) {
    gl_Position = MATRIX_MVP * vec4(in_Position, 1.0);
    pass_Normal = (NormalMatrix * vec4(in_Normal, 0.0)).xyz;
    pass_Color = in_Color * Color;
    pass_Position = MATRIX_MV * vec4(in_Position, 1.0);
}
