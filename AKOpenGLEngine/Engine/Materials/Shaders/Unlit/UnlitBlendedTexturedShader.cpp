//
//  UnlitBlendedTexturedShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 02.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "UnlitBlendedTexturedShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../VertexAttrib.h"

using namespace std;

namespace AKOpenGLEngine {
    string UnlitBlendedTexturedShader::getShaderName() const {
        return "UnlitBlendedTexturedShader";
    }

    void UnlitBlendedTexturedShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribPosition, "in_Position"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribUV, "in_UV"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribColor, "in_Color"));
    }

    void UnlitBlendedTexturedShader::fetchUniformLocations() {
        uniforms.push_back("MATRIX_MVP");
        uniforms.push_back("texture0");
        uniforms.push_back("texture1");
        uniforms.push_back("blendFactor");
        SurfaceShader::fetchUniformLocations();
    }

    bool UnlitBlendedTexturedShader::isUsingLight() {
        return false;
    }
}