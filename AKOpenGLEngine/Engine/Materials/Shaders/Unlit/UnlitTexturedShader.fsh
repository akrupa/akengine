#version 410 core

in vec4 pass_Color;
in vec2 pass_UV;

layout(location = 0) out vec4 out_Color;

uniform sampler2D texture0;

void main(void) {
	out_Color = pass_Color * texture( texture0, pass_UV );
}