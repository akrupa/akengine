//
//  UnlitTexturedShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 01.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __UnlitTexturedShader_H_
#define __UnlitTexturedShader_H_

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class UnlitTexturedShader : public SurfaceShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}


#endif //__UnlitTexturedShader_H_
