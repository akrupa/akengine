//
//  UnlitShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 28.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __UnlitShader_H_
#define __UnlitShader_H_

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class UnlitShader : public SurfaceShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}


#endif //__UnlitShader_H_
