//
//  GeometryShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __GeometryShader_H_
#define __GeometryShader_H_

#include "Shader.h"

namespace AKOpenGLEngine {
    class GeometryShader : public Shader {

        virtual std::string getShaderName() const = 0;

        virtual void bindAttributeLocations() = 0;

    public:
        virtual void setup();
    };
}

#endif //__GeometryShader_H_
