//
//  Shader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 19.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __Shaders_H_
#define __Shaders_H_

#include <string>
#include <map>

#include "../../Utilities/Singleton.h"

namespace AKOpenGLEngine {

    class Shader;

    class Shaders : public Singleton<Shaders> {

        std::map<std::string, Shader*> shadersMap;

    public:
        void ReleaseShaders();

        Shader *GetShader(std::string shaderName);
        Shader *GetAmbientShader();
        Shader *GetAmbientTexturedShader();
        Shader *GetDebugNormalsShader();
        Shader *GetUnlitBlendedTexturedShader();
        Shader *GetUnlitShader();
        Shader *GetUnlitTexturedShader();
        Shader *GetDiffuseShader();
        Shader *GetDiffuseSpecularShader();
        Shader *GetDiffuseTexturedShader();
        Shader *GetDiffuseSpecularTexturedShader();
        Shader *GetNormalDiffuseShader();
        Shader *GetNormalDiffuseSpecularShader();
        Shader *GetUnlitParticlesShader();
        Shader *GetUnlitParticlesTexturedShader();
        Shader *GetPostProcessingShader();
        Shader *GetSkyBoxShader();
        Shader *GetSchwarzschildBlackHoleSkyBox();
    };
}


#endif //__Shaders_H_
