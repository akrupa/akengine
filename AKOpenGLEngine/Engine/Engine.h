//
//  Engine.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 15.05.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __AKOpenGLEngine__Engine__
#define __AKOpenGLEngine__Engine__

#include <stdio.h>

namespace AKOpenGLEngine {
        
    class Engine {
        
    public:
        void CreateWindow();
        void Run();
        
    };
}

#endif /* defined(__AKOpenGLEngine__Engine__) */
