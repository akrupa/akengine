//
//  Engine.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 15.05.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "Engine.h"
#include "Renderer.h"

namespace AKOpenGLEngine {
    
    void Engine::CreateWindow() {
        Renderer::getInstance().CreateWindow();
    }
    
    void Engine::Run() {
        Renderer::getInstance().Run();
    }

}