//
//  GLDebug.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 10.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __GLDebug_h
#define __GLDebug_h

#include <easylogging++/easylogging++.h>
#include <string>

#if defined(_DEBUG)

    std::string getOpenGLErrorCodeFromEnum(unsigned int errorCodeEnum);

    #define \
        GL_CHECK(stmt)\
        do {\
            GLenum err;\
            std::string ERR;\
            while((err = glGetError()) != GL_NO_ERROR) {\
                LOG(ERROR)<<getOpenGLErrorCodeFromEnum(err)<<" (previous GL call) "<<#stmt<<" "<<el::base::debug::StackTrace();\
            }\
            stmt;\
            while((err = glGetError()) != GL_NO_ERROR) {\
                LOG(ERROR)<<getOpenGLErrorCodeFromEnum(err)<<" "<<#stmt<<" "<<el::base::debug::StackTrace();\
            }\
        } while (0)

    #define \
        GL_CHECK_RETURN(stmt)\
        do {\
            GLenum err;\
            while((err = glGetError()) != GL_NO_ERROR) {\
                LOG(ERROR)<<getOpenGLErrorCodeFromEnum(err)<<" (previous GL call) "<<#stmt<<" "<<el::base::debug::StackTrace();\
            }\
        } while(0);\
        stmt;\
        do {\
            GLenum err;\
            while((err = glGetError()) != GL_NO_ERROR) {\
                LOG(ERROR)<<getOpenGLErrorCodeFromEnum(err)<<" "<<#stmt<<" "<<el::base::debug::StackTrace();\
            }\
        } while(0);


    #define \
        GL_CHECK_CLEAR()\
        do {\
            GLenum err;\
            while((err = glGetError()) != GL_NO_ERROR);\
        } while(0);
#else
    #define GL_CHECK(stmt) stmt
    #define GL_CHECK_RETURN(stmt) stmt
    #define GL_CHECK_CLEAR() ;
#endif

#endif
