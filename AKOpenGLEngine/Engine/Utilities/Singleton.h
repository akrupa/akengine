//
//  Singleton.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 05.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Singleton_H_
#define __Singleton_H_

namespace AKOpenGLEngine {

    template <typename T>
    class Singleton {

    public:
        static T& getInstance() {
            static T me;
            return me;
        }
    protected:
        Singleton() {};
        Singleton(Singleton const&) = delete;
        void operator=(Singleton const&) = delete;

    };
}


#endif //__Singleton_H_
