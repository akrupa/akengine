//
//  OpenGL.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 14.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef AKOpenGLEngine_OpenGL_h
#define AKOpenGLEngine_OpenGL_h

#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
#include <OpenGLES/ES3/glext.h>
#else
#include <GL/glew.h>
#endif

#endif
