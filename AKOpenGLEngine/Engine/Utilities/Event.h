//
//  Event.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 25.05.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __AKOpenGLEngine__Event__
#define __AKOpenGLEngine__Event__

#include <functional>
#include <vector>

template<typename Ret, typename... Types>
class Event
{
public:
    typedef std::function<Ret(Types...)> Func;
    
public:
    void Call( Types... args ) {
        for( auto i = m_handlers.begin(); i != m_handlers.end(); i++ )
        {
            (*i)( args... );
        }
    }
    
    void operator()( Types... args) {
        Call(args...);
    }
    
    Event& operator+= (Func f) {
        m_handlers.push_back(f);
        return *this;
    }
    
    Event& operator-= (Func f) {
        
        for( auto i = m_handlers.begin(); i != m_handlers.end(); i++ )
        {
            if ( (i)->template target<Ret(Types...)>() == (&f)->template target<Ret(Types...)>() )
            {
                m_handlers.erase( i );
                break;
            }
        }
        
        return *this;
    }
private:
    std::vector<Func> m_handlers;
     
};

#endif /* defined(__AKOpenGLEngine__Event__) */

