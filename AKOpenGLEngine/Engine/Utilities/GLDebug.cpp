//
//  GLDebug.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 24.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "Utilities/GLDebug.h"
#include "Utilities/OpenGL.h"

std::string getOpenGLErrorCodeFromEnum(unsigned int errorCodeEnum) {
    
    switch (errorCodeEnum) {
            
        case GL_NO_ERROR:
            return "GL_NO_ERROR";
            break;
            
        case GL_INVALID_ENUM:
            return "GL_INVALID_ENUM";
            break;
            
        case GL_INVALID_VALUE:
            return "GL_INVALID_VALUE";
            break;
            
        case GL_INVALID_OPERATION:
            return "GL_INVALID_OPERATION";
            break;
            
#if !TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR
            
        case GL_STACK_OVERFLOW:
            return "GL_STACK_OVERFLOW";
            break;
            
        case GL_STACK_UNDERFLOW:
            return "GL_STACK_UNDERFLOW";
            break;
#endif

        case GL_INVALID_FRAMEBUFFER_OPERATION:
            return "GL_INVALID_FRAMEBUFFER_OPERATION";
            break;
            
        case GL_OUT_OF_MEMORY:
            return "GL_OUT_OF_MEMORY";
            break;
            
        default:
            return std::string(std::to_string(errorCodeEnum));
            break;
    }
    
}
