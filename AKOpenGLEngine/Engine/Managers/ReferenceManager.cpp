//
//  ReferenceManager.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 07.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <iostream>
#include <yaml-cpp/yaml.h>

#include "ReferenceManager.h"
#include "../Materials/Meshes/Mesh.h"
#include "../Materials/Material.h"
#include "../Materials/Texture.h"
#include "../Materials/Shaders/Shader.h"

using namespace glm;
using namespace std;
using namespace YAML;

namespace AKOpenGLEngine {

    Emitter &operator<<(Emitter &out, const vec4& v) {
        out << Flow << YAML::BeginSeq << v.x << v.y << v.z << v.w << EndSeq;
        return out;
    }

    Emitter &operator<<(Emitter &out, const vec3& v) {
        out << Flow << BeginSeq << v.x << v.y << v.z << EndSeq;
        return out;
    }
    
    Emitter &operator<<(Emitter &out, const vec2& v) {
        out << Flow << BeginSeq << v.x << v.y << EndSeq;
        return out;
    }

    Emitter &operator<<(Emitter &out, const quat& q) {
        out << Flow << BeginSeq << q.x << q.y << q.z << q.w << EndSeq;
        return out;
    }

    Emitter &operator<<(Emitter &out, const glm::mat4& m) {
        out << Flow << BeginSeq;
        for(int i=0;i<4;i++) {
            for (int j = 0; j < 4; ++j) {
                out<<m[i][j];
            }
        }
        out<<EndSeq;
        return out;
    }

    void *ReferenceManager::GetReference(long long int reference) {
        return references[reference];
    }

    void ReferenceManager::RegisterMeshReference(Mesh *reference) {
        meshes.insert(reference);
    }

    void ReferenceManager::RegisterShaderReference(Shader *reference) {
        shaders.insert(reference);
    }

    void ReferenceManager::RegisterMaterialReference(Material *reference) {
        materials.insert(reference);
    }

    void ReferenceManager::printYaml(Emitter &out) {
        out << Key << "Meshes" << Value << meshes;
        out << Key << "Materials" << Value << materials;
        out << Key << "Textures" << Value << textures;
        out << Key << "Shaders" << Value << shaders;
    }

    void ReferenceManager::RegisterTextureReference(Texture *reference) {
        textures.insert(reference);
    }

    void ReferenceManager::RegisterReference(long long int reference, void *currentReference) {
        if(currentReference == nullptr) {
            return;
        }
        references.insert(pair<long long int, void*>(reference, currentReference));
    }
}
