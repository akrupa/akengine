//
//  InputManager.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 22.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <SDl2/SDL.h>
#include <iostream>

#include "InputManager.h"

using namespace glm;
using namespace std;

namespace AKOpenGLEngine {
    
    MOUSE_BUTTON InputManager::GetMouseButton(uint8_t sdlMouse) const {
        if (sdlMouse == SDL_BUTTON_LEFT) {
            return MOUSE_BUTTON::LEFT;
        } else if (sdlMouse == SDL_BUTTON_RIGHT) {
            return MOUSE_BUTTON::RIGHT;
        } else if (sdlMouse == SDL_BUTTON_MIDDLE) {
            return MOUSE_BUTTON::MIDDLE;
        }
        return MOUSE_BUTTON::LEFT;
    }
    
    uint16_t InputManager::GetKeyboardButton(KEYBOARD_BUTTON button) const {
        switch (button) {
            case KEYBOARD_BUTTON::KEY_SPACE: return SDL_SCANCODE_SPACE;
            case KEYBOARD_BUTTON::KEY_LEFT_SHIFT: return SDL_SCANCODE_LSHIFT;
            case KEYBOARD_BUTTON::KEY_RIGHT_SHIFT: return SDL_SCANCODE_RSHIFT;
            case KEYBOARD_BUTTON::KEY_A: return SDL_SCANCODE_A;
            case KEYBOARD_BUTTON::KEY_B: return SDL_SCANCODE_B;
            case KEYBOARD_BUTTON::KEY_C: return SDL_SCANCODE_C;
            case KEYBOARD_BUTTON::KEY_D: return SDL_SCANCODE_D;
            case KEYBOARD_BUTTON::KEY_E: return SDL_SCANCODE_E;
            case KEYBOARD_BUTTON::KEY_F: return SDL_SCANCODE_F;
            case KEYBOARD_BUTTON::KEY_G: return SDL_SCANCODE_G;
            case KEYBOARD_BUTTON::KEY_H: return SDL_SCANCODE_H;
            case KEYBOARD_BUTTON::KEY_I: return SDL_SCANCODE_I;
            case KEYBOARD_BUTTON::KEY_J: return SDL_SCANCODE_J;
            case KEYBOARD_BUTTON::KEY_K: return SDL_SCANCODE_K;
            case KEYBOARD_BUTTON::KEY_L: return SDL_SCANCODE_L;
            case KEYBOARD_BUTTON::KEY_M: return SDL_SCANCODE_M;
            case KEYBOARD_BUTTON::KEY_N: return SDL_SCANCODE_N;
            case KEYBOARD_BUTTON::KEY_O: return SDL_SCANCODE_O;
            case KEYBOARD_BUTTON::KEY_P: return SDL_SCANCODE_P;
            case KEYBOARD_BUTTON::KEY_Q: return SDL_SCANCODE_Q;
            case KEYBOARD_BUTTON::KEY_R: return SDL_SCANCODE_R;
            case KEYBOARD_BUTTON::KEY_S: return SDL_SCANCODE_S;
            case KEYBOARD_BUTTON::KEY_T: return SDL_SCANCODE_T;
            case KEYBOARD_BUTTON::KEY_U: return SDL_SCANCODE_U;
            case KEYBOARD_BUTTON::KEY_V: return SDL_SCANCODE_V;
            case KEYBOARD_BUTTON::KEY_W: return SDL_SCANCODE_W;
            case KEYBOARD_BUTTON::KEY_X: return SDL_SCANCODE_X;
            case KEYBOARD_BUTTON::KEY_Y: return SDL_SCANCODE_Y;
            case KEYBOARD_BUTTON::KEY_Z: return SDL_SCANCODE_Z;
        }
        return SDL_SCANCODE_BRIGHTNESSDOWN;
    }
    
    void InputManager::GetMousePosition(int *x, int *y) const {
        SDL_GetMouseState(x, y);
    }
    
    vec2 InputManager::GetMousePosition() const {
        int x;
        int y;
        GetMousePosition(&x, &y);
        return vec2(x,y);
    }
    
    bool InputManager::isTouchSupported() const {
        return SDL_GetNumTouchDevices>0;
    }
    
    bool InputManager::GetKeyboardButtonState(KEYBOARD_BUTTON button) const {
        return SDL_GetKeyboardState(NULL)[GetKeyboardButton(button)];
    }
    
    bool InputManager::GetMouseButtonState(MOUSE_BUTTON button) const {
        if (button == MOUSE_BUTTON::LEFT) {
            return SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT);
        } else if (button == MOUSE_BUTTON::RIGHT) {
            return SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT);
        } else if (button == MOUSE_BUTTON::MIDDLE) {
            return SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_MIDDLE);
        }
        return false;
    }
    
    int InputManager::GetTouchCount() const {
        return SDL_GetNumTouchFingers(0);
    }
    
    void InputManager::GetTouchPosition(int fingerNumber, int *x, int *y) const {
        auto finger = SDL_GetTouchFinger(0, fingerNumber);
        *x = finger -> x;
        *y = finger -> y;
    }
    
    void InputManager::processSDLEvents(void *events) {
        SDL_Event *sdlEvent = (SDL_Event *)events;
        switch (sdlEvent -> type) {
            case SDL_MOUSEBUTTONDOWN: {
                SDL_MouseButtonEvent *mouseButtonEvent = (SDL_MouseButtonEvent *)sdlEvent;
                mouseButtonDown(GetMouseButton(mouseButtonEvent->button), mouseButtonEvent->x, mouseButtonEvent->y);
                break;
            }
                
            case SDL_KEYDOWN: {
                SDL_KeyboardEvent *keyboardEvent = (SDL_KeyboardEvent *)sdlEvent;
                keyboardButtonDown(keyboardEvent->keysym.sym);
                break;
            }
        }
    }

}
