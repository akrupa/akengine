//
//  ModelManager.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 03.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <vector>
#include <easylogging++/easylogging++.h>

#include "ModelManager.h"
#include "../Utilities/WorkingDirectory.h"
#include "../Materials/Meshes/Mesh.h"
#include "../Materials/Meshes/TrianglesMesh.h"
#include "../Materials/Meshes/Triangle.h"

using namespace std;
using namespace glm;

namespace AKOpenGLEngine {

    ModelManager::ModelManager() {

    }

    Mesh *AKOpenGLEngine::ModelManager::LoadMesh(string filename) {

        Assimp::Importer importer;
        string file = WorkingDirectory::GetExecutableDirectory() + "/Assets/models/" + filename;

        const aiScene *scene = importer.ReadFile(file,
                aiProcess_CalcTangentSpace |
                        aiProcess_Triangulate |
                        aiProcess_JoinIdenticalVertices |
                        aiProcess_SortByPType);

        // If the import failed, report it
        if( !scene)
        {
            LOG(ERROR) << "Couldn't load mesh: " + filename;
            return nullptr;
        }

        aiMesh *m = scene->mMeshes[0];
        vector<Vertex> vertices = vector<Vertex>();
        vector<Triangle> triangles = vector<Triangle>();
        for (unsigned int i = 0; i < m->mNumVertices; ++i) {
            Vertex v;
            v.position = vec3(m->mVertices[i].x, m->mVertices[i].y, m->mVertices[i].z);
            v.normal = vec3(m->mNormals[i].x, m->mNormals[i].y, m->mNormals[i].z);

            //if (m->mColors!=nullptr && m->mColors[i] != nullptr) {
            //    v.color = vec4(m->mColors[i]->r, m->mColors[i]->g, m->mColors[i]->b, m->mColors[i]->a);
            //} else {
                v.color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
            //}
            vertices.push_back(v);
        }

        for (unsigned int i = 0; i < m->mNumFaces; ++i) {
            Triangle t;

            t.indices.a = m->mFaces[i].mIndices[0];
            t.indices.b = m->mFaces[i].mIndices[1];
            t.indices.c = m->mFaces[i].mIndices[2];

            triangles.push_back(t);
        }

        Mesh *mesh = new TrianglesMesh(vertices, triangles);

        return mesh;
    }
}
