//
//  TextureManager.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 01.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <easylogging++/easylogging++.h>
#include <yaml-cpp/yaml.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "TextureManager.h"
#include "../Utilities/GLDebug.h"
#include "../Utilities/WorkingDirectory.h"
#include "../Materials/Texture.h"
#include "../Materials/CubeMapTexture.h"

using namespace std;
using namespace YAML;

namespace AKOpenGLEngine {

    Texture* TextureManager::LoadTexture(string filename, GLenum image_format, GLenum internal_format, GLint level, GLint border) {
        Texture* texture = nullptr;

        string file = WorkingDirectory::GetExecutableDirectory() + "/Assets/textures/" + filename;
        SDL_Surface* loadedSurface = IMG_Load( file.c_str() );
        if( loadedSurface == nullptr ) {
            LOG(ERROR) << "Unable to load image " << file << ". Error: " << IMG_GetError();
        } else {
            SDL_Surface* optimizedSurface = SDL_ConvertSurfaceFormat(loadedSurface, SDL_PIXELFORMAT_RGB24, 0);
            GLuint gl_texID;
            GL_CHECK(glGenTextures(1, &gl_texID));
            m_texID[gl_texID] = gl_texID;
            GL_CHECK(glBindTexture(GL_TEXTURE_2D, gl_texID));
            
            GL_CHECK(glTexImage2D(GL_TEXTURE_2D, level, internal_format, optimizedSurface->w, optimizedSurface->h,
                                  border, image_format, GL_UNSIGNED_BYTE, optimizedSurface->pixels));
            
            GL_CHECK(glGenerateMipmap(GL_TEXTURE_2D));
            GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT));
            GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT));
            GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
            GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
            
            SDL_FreeSurface( optimizedSurface );
            SDL_FreeSurface( loadedSurface );
            
            texture = new Texture(gl_texID, filename, image_format, internal_format, level, border);
        }
        return texture;
    }

    Texture* TextureManager::LoadTexture(Node textureNode) {
        string filename = textureNode["name"].as<string>();
        GLenum image_format = textureNode["image_format"].as<GLenum>();
        GLenum internal_format = textureNode["internal_format"].as<GLenum>();
        GLint level = textureNode["level"].as<GLint>();
        GLint border = textureNode["border"].as<GLint>();
        return LoadTexture(filename, image_format, internal_format, level, border);
    }
    
    CubeMapTexture* TextureManager::LoadCubeMapTexture(vector<string> filenames, GLuint width, GLuint height, GLenum image_format) {
        CubeMapTexture* texture = nullptr;
        
        GLuint gl_texID;
        GL_CHECK(glGenTextures(1, &gl_texID));
        GL_CHECK(glBindTexture(GL_TEXTURE_CUBE_MAP, gl_texID));

#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
        GL_CHECK(glTexStorage2D(GL_TEXTURE_CUBE_MAP, 10, GL_RGB8, width, height));
#else
        GL_CHECK(glTexStorage2D(GL_TEXTURE_CUBE_MAP, 10, GL_RGB16, width, height));
#endif
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        
#if !TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR
        glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
#endif
        
        SDL_Surface* loadedSurfaces[6];
        SDL_Surface* optimizedSurfaces[6];
        for (int i=0; i<6; i++) {
            string file = WorkingDirectory::GetExecutableDirectory() + "/Assets/textures/" + filenames[i];
            loadedSurfaces[i] = IMG_Load( file.c_str() );

            if( loadedSurfaces[i] == nullptr ) {
                LOG(ERROR) << "Unable to load image " << file << ". Error: " << IMG_GetError();
            } else {
                optimizedSurfaces[i] = SDL_ConvertSurfaceFormat(loadedSurfaces[i], SDL_PIXELFORMAT_RGB24, 0);

                GLenum target = GL_TEXTURE_CUBE_MAP_POSITIVE_X + i;
                GL_CHECK(glTexSubImage2D(target, 0, 0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, optimizedSurfaces[i]->pixels));
            }
        }
        
        texture = new CubeMapTexture(gl_texID, "", image_format);
        for (int i=0; i<6; i++) {
            SDL_FreeSurface( loadedSurfaces[i] );
            SDL_FreeSurface( optimizedSurfaces[i] );
        }
        
        return texture;
    }

    bool TextureManager::UnloadTexture(const unsigned int texID) {
        bool result(true);
        if (m_texID.find(texID) != m_texID.end()) {
            glDeleteTextures(1, &(m_texID[texID]));
            m_texID.erase(texID);
        }
        else {
            result = false;
        }

        return result;
    }

    bool TextureManager::BindTexture(const Texture *tex) {
        bool result(true);
        unsigned int texID = (unsigned int) tex->GetBind();
        if (m_texID.find(texID) != m_texID.end()) {
            GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_texID[texID]));
        } else {
            result = false;
        }

        return result;
    }

    void TextureManager::UnloadAllTextures() {
        map<unsigned int, GLuint>::iterator i = m_texID.begin();
        while (i != m_texID.end()) {
            UnloadTexture(i->first);
        }
        m_texID.clear();
    }

    void TextureManager::SaveTextureToPNG(std::string filename, int width, int height, unsigned int framebuffer) {
        GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, framebuffer));

        GLubyte *pixels = (GLubyte *) malloc(4 * width * height * sizeof(GLubyte));

        GL_CHECK(glReadPixels(0, 0, width, height, GL_BGRA, GL_UNSIGNED_BYTE, pixels));
        GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0));
        
        SDL_Surface * surf = SDL_CreateRGBSurfaceFrom(pixels, width, height, 8*4, width*4, 0,0,0,0);
        if(SDL_SaveBMP(surf, ("/Users/adrian/Library/Developer/Xcode/DerivedData/Engine-axwpfnvmuvrqclbrgotmrgfiuvhl/Build/Products/Release/resources/textures/" + filename).c_str()) != 0 ) {
            std::cerr << "SDL_SaveBMP() Failed." << std::endl;
        }
        printf("Saving to %s\n", (WorkingDirectory::GetExecutableDirectory() + "/Assets/textures/" + filename).c_str());
        SDL_FreeSurface(surf);
        free(pixels);

        //FIBITMAP *Image = FreeImage_ConvertFromRawBits(pixels, width, height, 4 * width, 32, 0x0000FF, 0x00FF00, 0xFF0000, false);
        //FreeImage_Save(FIF_PNG, Image, (WorkingDirectory::GetExecutableDirectory() + "/resources/textures/" + filename).c_str(), 0);
    }


}
