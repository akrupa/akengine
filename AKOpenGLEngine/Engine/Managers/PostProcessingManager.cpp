//
//  PostProcessingManager.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 02.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
#include <OpenGLES/ES3/glext.h>
#else
#include <GL/glew.h>
#endif

#include "PostProcessingManager.h"
#include "../Utilities/GLDebug.h"
#include "../Components/Camera.h"
#include "../Materials/Material.h"
#include "../Materials/Shaders/Shader.h"
#include "../Materials/Shaders/Shaders.h"
#include "../Materials/RenderTexture.h"
#include "../Materials/Shaders/VertexAttrib.h"

namespace AKOpenGLEngine {
 
    PostProcessingManager::PostProcessingManager() {
        postprocessingCamera = new Camera();
        
        GL_CHECK(glGenVertexArrays(1, &fullscreenFBO));
        GL_CHECK(glBindVertexArray(fullscreenFBO));
        
        GLfloat bufferData[] = {
            -1.0f, -1.0f, 0.0f,
            1.0f, -1.0f, 0.0f,
            -1.0f,  1.0f, 0.0f,
            -1.0f,  1.0f, 0.0f,
            1.0f, -1.0f, 0.0f,
            1.0f,  1.0f, 0.0f,
        };
        
        GLuint quad_vertexbuffer;
        GL_CHECK(glGenBuffers(1, &quad_vertexbuffer));
        GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer));
        GL_CHECK(glBufferData(GL_ARRAY_BUFFER, sizeof(bufferData), bufferData, GL_STATIC_DRAW));
        
        GL_CHECK(glEnableVertexAttribArray(VertexAttribPosition));
                GL_CHECK(glVertexAttribPointer(VertexAttribPosition,3,GL_FLOAT, GL_FALSE, 0,(void*)0));
        shader = Shaders::getInstance().GetPostProcessingShader();
        
        GL_CHECK_RETURN(textureUniform = glGetUniformLocation(shader->getProgram(), "renderedTexture"));
    }
    
    void PostProcessingManager::Render() {
        if (renderTexture == nullptr || shader == nullptr) {
            return;
        }
        Material::ResetMaterial();
        
        postprocessingCamera->PrepareCameraForRendering();
        postprocessingCamera->Clear();
        GL_CHECK(glBindVertexArray(fullscreenFBO));

        GL_CHECK(glUseProgram(shader->getProgram()));
        GL_CHECK(glActiveTexture(GL_TEXTURE0));
        GL_CHECK(glBindTexture(GL_TEXTURE_2D, renderTexture->GetBind()));
        GL_CHECK(glUniform1i(textureUniform, 0));
        
        GL_CHECK(glDepthMask(GL_TRUE));
        GL_CHECK(glDepthFunc(GL_LEQUAL));
        GL_CHECK(glDisable(GL_BLEND));
        GL_CHECK(glDrawArrays(GL_TRIANGLES, 0, 6));
    }
    
    void PostProcessingManager::setViewport(int x, int y, int width, int height) {
        postprocessingCamera->setViewport(glm::vec4(x,y,width,height));
    }
    
    void PostProcessingManager::setRenderTexture(RenderTexture *renderTexture) {
        this->renderTexture = renderTexture;
    }

}