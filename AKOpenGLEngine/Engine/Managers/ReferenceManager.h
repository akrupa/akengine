//
//  ReferenceManager.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 07.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __ReferenceManager_H_
#define __ReferenceManager_H_

#include <map>
#include <string>
#include <set>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include "../Utilities/Singleton.h"

namespace YAML {
    class Emitter;
}

namespace AKOpenGLEngine {

    class Mesh;
    class Material;
    class Texture;
    class Shader;

    YAML::Emitter& operator << (YAML::Emitter& out, const glm::vec4& v);
    YAML::Emitter& operator << (YAML::Emitter& out, const glm::vec3& v);
    YAML::Emitter& operator << (YAML::Emitter& out, const glm::vec2& v);
    YAML::Emitter& operator << (YAML::Emitter& out, const glm::quat& q);
    YAML::Emitter& operator << (YAML::Emitter& out, const glm::mat4& m);

    class ReferenceManager : public Singleton<ReferenceManager> {

        std::map<long long int, void*> references;
        std::set<Mesh*>meshes;
        std::set<Material*>materials;
        std::set<Texture*>textures;
        std::set<Shader*>shaders;

    public:

        void* GetReference(long long int reference);
        void RegisterReference(long long int reference, void* currentReference);
        void RegisterMeshReference(Mesh* reference);
        void RegisterShaderReference(Shader* reference);
        void RegisterMaterialReference(Material* reference);
        void RegisterTextureReference(Texture* texture);

        void printYaml(YAML::Emitter& out);

    };
}


#endif //__ReferenceManager_H_
