//
//  InputManager.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 22.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __InputManager_H_
#define __InputManager_H_

#include <glm/glm.hpp>
#include <cstdint>

#include "../Utilities/Singleton.h"
#include "../Utilities/Event.h"


namespace AKOpenGLEngine {
    
    enum class MOUSE_BUTTON {
        LEFT,
        RIGHT,
        MIDDLE
    };
    
    enum class KEYBOARD_BUTTON {
        KEY_SPACE = ' ',
        KEY_LEFT_SHIFT,
        KEY_RIGHT_SHIFT,
        KEY_A = 'a',
        KEY_B = 'B',
        KEY_C = 'C',
        KEY_D = 'D',
        KEY_E = 'E',
        KEY_F = 'F',
        KEY_G = 'G',
        KEY_H = 'H',
        KEY_I = 'I',
        KEY_J = 'J',
        KEY_K = 'K',
        KEY_L = 'L',
        KEY_M = 'M',
        KEY_N = 'N',
        KEY_O = 'O',
        KEY_P = 'P',
        KEY_Q = 'Q',
        KEY_R = 'R',
        KEY_S = 'S',
        KEY_T = 'T',
        KEY_U = 'U',
        KEY_V = 'V',
        KEY_W = 'W',
        KEY_X = 'X',
        KEY_Y = 'Y',
        KEY_Z = 'Z',
    };
    

    class InputManager : public Singleton<InputManager> {
        
        MOUSE_BUTTON GetMouseButton(uint8_t sdlMouse) const;
        uint16_t GetKeyboardButton(KEYBOARD_BUTTON button) const;
        
    public:
        void GetMousePosition(int *x, int *y) const;
        glm::vec2 GetMousePosition() const;
        bool GetMouseButtonState(MOUSE_BUTTON button) const;
        
        bool isTouchSupported() const;
        
        bool GetKeyboardButtonState(KEYBOARD_BUTTON button) const;
        
        int GetTouchCount() const;
        void GetTouchPosition(int fingerNumber, int *x, int *y) const;
        
        Event<void, MOUSE_BUTTON, int, int> mouseButtonDown;
        
        Event<void, char> keyboardButtonDown;
        void processSDLEvents(void *events);
        
        
        
    };
}


#endif //__InputManager_H_
