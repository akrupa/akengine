//
//  RendererManager.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 05.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __RendererManager_H_
#define __RendererManager_H_

#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
#include <OpenGLES/ES3/glext.h>
#else
#include <GL/glew.h>
#endif
#include <vector>

#include "../Utilities/Singleton.h"
#include "../Components/Light.h"

namespace AKOpenGLEngine {

    class Camera;

    class RendererManager : public Singleton<RendererManager> {

        GLuint lightsBufferObject;
        GLubyte * lightsBlockBuffer;
        GLint lightsBlockSize;
        GLint *lightBlockOffsets;

        int frameHash;
        
        GLuint defaultFramebuffer;

        Camera *currentCamera;
        unsigned short currentRenderPass = 0;

    public:
        RendererManager();

        GLuint getLightsBufferObject();
        void prepareLights(std::vector<LightStruct> lights, Camera* camera);
        void startNewFrame();
        void startRenderingWithCamera(Camera *camera);
        int getFrameHash();
        Camera *getCurrentCamera();
        unsigned short getRenderPass();
        float shadingLanguageVersion = 1.0f;
        void NextRenderPass();
        void setDefaultFramebuffer(GLuint defaultFramebuffer);
        GLuint getDefaultFramebuffer();
    };
}


#endif //__RendererManager_H_
