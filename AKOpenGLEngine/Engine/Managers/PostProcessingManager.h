//
//  PostProcessingManager.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 02.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __AKOpenGLEngine__PostProcessingManager__
#define __AKOpenGLEngine__PostProcessingManager__

#include <memory>
#include <vector>

#include "../Utilities/Singleton.h"

namespace AKOpenGLEngine {
    
    class Camera;
    class Shader;
    class RenderTexture;
    
    class PostProcessingManager : public Singleton<PostProcessingManager> {
        
        Camera *postprocessingCamera;
        GLuint fullscreenFBO;
        Shader *shader;
        GLuint textureUniform;
        
        RenderTexture *renderTexture;
        
        std::vector<std::shared_ptr<RenderTexture>> renderingTextures;
        
    public:
        PostProcessingManager();
        void Render();
        void setViewport(int x, int y, int width, int height);
        void setRenderTexture(RenderTexture *renderTexture);
        
        std::vector<std::shared_ptr<RenderTexture>> requestTextures();
    };
}

#endif /* defined(__AKOpenGLEngine__PostProcessingManager__) */
