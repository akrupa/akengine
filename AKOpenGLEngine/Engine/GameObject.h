//
//  GameObject.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __GameObject_H_
#define __GameObject_H_

#include <vector>
#include <typeinfo>
#include <string>
#include <type_traits>

namespace YAML {
    class Node;
    class Emitter;
}

//TODO: clean class

namespace AKOpenGLEngine {

    class Camera;

    class Transform;

    class Component;

    class Light;
    
    class MeshRenderer;

    class GameObject {

        bool isEnabled;

        std::vector<Component *> components;
        Transform *transform;
        std::string name;
        MeshRenderer *cachedMeshRenderer = nullptr;

    public:
        GameObject();

        GameObject(YAML::Node gameObjectNode);

        void Update(float dt);

        void FixedUpdate(float dt);
        
        void OnDestroy();

        void Render(unsigned int renderPass, Camera *camera, bool useLight = false);

        Transform *GetTransform() const;

        GameObject* AddComponent(Component *component);

        bool HasComponentOfType(std::string type) const;

        std::string GetName() const;

        std::vector<Component *> GetComponents() const;

        void SetName(std::string name);

        Component *GetComponentOfType(std::string type) const;

        template<typename T>
        T *GetComponent() {
            static_assert(std::is_base_of<Component, T>::value,
                    "T must be a descendant of Component"
            );
            for (auto &component : components) {

                T *t = dynamic_cast<T *> (component);
                if (t != nullptr) {
                    return t;
                }
            }
            return nullptr;
        }

        template<typename T>
        std::vector<T> *GetComponents() {
            static_assert(std::is_base_of<Component, T>::value,
                          "T must be a descendant of Component"
                          );
            std::vector<T> v;
            for (auto &component : components) {
                
                T *t = dynamic_cast<T *> (component);
                if (t != nullptr) {
                    v.push_back(t);
                }
            }
            return v;
        }
        
        GameObject* RemoveComponent(Component *component);

        static GameObject *CreateCamera();

        static GameObject *CreateDirectionalLight();

        static GameObject *CreatePointLight();

        static GameObject *CreateSpotLight();

        bool operator<(GameObject &obj);

        friend YAML::Emitter &operator<<(YAML::Emitter &out, const GameObject *e);
    };

}

#endif //__GameObject_H_
