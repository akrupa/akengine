//
//  Component.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <easylogging++/easylogging++.h>
#include <yaml-cpp/yaml.h>

#include "Components/Component.h"
#include "GameObject.h"
#include "Components/Camera.h"
#include "../Scene.h"

using namespace YAML;

namespace AKOpenGLEngine {
    
#pragma Private methods

    void Component::AssignGameObject(GameObject *gameObject) {
        if (this->gameObject == nullptr) {
            this->gameObject = gameObject;
            Start();
        } else {
            this->gameObject = gameObject;
        }
    }
    
    std::vector<Component *> Component::GetAllComponentsInScene() {
        std::vector<Component *>  v;
        
        auto gameObjects = Scene::instance->getGameObjects();
        for(auto &gameObject : gameObjects) {
            auto components = gameObject->GetComponents();
            v.insert(v.end(), components.begin(), components.end());
        }
        
        return v;
    }

    
#pragma mark Protected constructors

    Component::Component(GameObject *gameObject) {
        this->gameObject = gameObject;
    }
    
    Component::Component() {
    }

#pragma mark Virtual methods
    
    Component::~Component() {
        gameObject = nullptr;
    }
    
    void Component::printYaml(Emitter &out) const {
        out << Key << "name" << Value << GetName();
        out << Key << "id" << Value << (long long int) this;
    }
    
    void Component::Start() {
        if (gameObject == nullptr) {
            LOG(ERROR) << "Component " << GetName() << " has no game object";
        }
    }
    
    void Component::OnDestroy() {
        delete this;
    }
    
    void Component::Update(float dt) {
        (void) dt;
        if (gameObject == nullptr) {
            LOG(ERROR) << "Component " << GetName() << " has no game object";
        }
        if (!isEnabled)
            return;
    }
    
    void Component::FixedUpdate(float dt) {
        (void) dt;
        if (gameObject == nullptr) {
            LOG(ERROR) << "Component " << GetName() << " has no game object";
        }
        if (!isEnabled)
            return;
    }
    
    void Component::Render(unsigned int renderPass, Camera *camera, bool useLight ) {
        (void) renderPass;
        (void) camera;
        (void) useLight;
    }
    
#pragma mark Public methods
    
    bool Component::IsEnabled() const {
        return isEnabled;
    }
    
    void Component::SetActive(bool active) {
        isEnabled = active;
    }
    
    Transform *Component::GetTransform() const {
        return gameObject->GetTransform();
    }
    
#pragma mark Friend method

    Emitter &operator<<(Emitter &out, const Component *c) {
        out << BeginMap;
        c->printYaml(out);
        out << EndMap;
        return out;
    }
}