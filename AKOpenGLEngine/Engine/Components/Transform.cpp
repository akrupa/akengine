//
//  Transform.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <glm/gtc/matrix_transform.hpp>
#include <yaml-cpp/yaml.h>

#include "Components/Transform.h"
#include "Components/Camera.h"
#include "Managers/ReferenceManager.h"
#include "Managers/RendererManager.h"

using namespace glm;
using namespace YAML;

namespace AKOpenGLEngine {

#pragma mark Constructors

    Transform::Transform() : Component() {
        isUnique = true;
        this->position = vec3(0.0f, 0.0f, 0.0f);
        this->scale = vec3(1.0f, 1.0f, 1.0f);
        this->rotation = quat(1.0f, 0.0f, 0.0f, 0.0f);
        transformChanged = true;
        localFrameHash = 0;
        modelMatrixChanged = false;
    }

    Transform::Transform(Node node) : Transform() {
        Node positionNode = node["position"];
        vec3 vPos;
        for (int i = 0; i < 3; i++) {
            vPos[i] = positionNode[i].as<float>();
        }

        Node scaleNode = node["scale"];
        vec3 vScale;
        for (int i = 0; i < 3; i++) {
            vScale[i] = scaleNode[i].as<float>();
        }

        Node rotationNode = node["rotation"];
        quat vRot;
        for (int i = 0; i < 4; i++) {
            vRot[i] = rotationNode[i].as<float>();
        }

        position = vPos;
        scale = vScale;
        rotation = vRot;
        transformChanged = true;
        localFrameHash = 0;
        modelMatrixChanged = false;
    }

#pragma mark Getters

    vec3 Transform::getPosition() const {
        return position;
    }

    vec3 Transform::getScale() const {
        return scale;
    }

    quat Transform::getRotation() const {
        return rotation;
    }

    vec3 Transform::getRotationInEulerAngles() const {
        return rotationInEulerAngles;
    }

    mat4 Transform::getModelMatrix() const {
        recomputeMatrices();
        return cachedModelMatrix;
    }

    mat4 Transform::getNormalMatrix() const {
        bool _transformChanged = transformChanged;
        recomputeMatrices();
        int hash = RendererManager::getInstance().getFrameHash();
        if (_transformChanged || hash != localFrameHash) {
            localFrameHash = hash;
            Camera *currentCamera = RendererManager::getInstance().getCurrentCamera();
            if (currentCamera != nullptr) {
                mat4 cameraViewMatrix = currentCamera->getViewMatrix();
                if (scale.x == scale.y && scale.y == scale.z) {
                    cachedNormalMatrix = cameraViewMatrix*cachedModelMatrix ;
                } else {
                    cachedNormalMatrix = transpose(inverse(cameraViewMatrix*cachedModelMatrix));
                }
            }
        }
        return cachedNormalMatrix;
    }

    mat4 Transform::getViewMatrix() const {
        recomputeMatrices();
        if (modelMatrixChanged) {
            modelMatrixChanged = false;
            cachedViewMatrix = inverse(getModelMatrix());
        }
        return cachedViewMatrix;
    }

#pragma mark Setters

    void Transform::setPosition(vec3 position) {
        this->position = position;
        transformChanged = true;
    }

    void Transform::setScale(vec3 scale) {
        this->scale = scale;
        transformChanged = true;
    }

    void Transform::setRotation(quat rotation) {
        this->rotation = normalize(rotation);
        this->rotationInEulerAngles = eulerAngles(rotation);
        transformChanged = true;
    }

    void Transform::setRotation(vec3 eulerAngles) {
        this->rotationInEulerAngles = eulerAngles;
        this->rotation = normalize(quat(eulerAngles));
        transformChanged = true;
    }

#pragma mark Virtual methods

    std::string Transform::GetName() const {
        return "Transform";
    }

    void Transform::printYaml(Emitter &out) const {
        Component::printYaml(out);
        out << Key << "position" << Value << position;
        out << Key << "scale" << Value << scale;
        out << Key << "rotation" << Value << rotation;
    }

#pragma mark Private methods

    void Transform::recomputeMatrices() const {
        if (transformChanged) {
            transformChanged = false;
            cachedModelMatrix =  translate(mat4(), position) * mat4_cast(rotation) * glm::scale(mat4(), scale);
            modelMatrixChanged = true;
        }
    }
}