//
//  NBodyParticleSystem.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 11.05.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <algorithm>
#include <glm/gtc/random.hpp>
#include <glm/gtx/norm.hpp>
#include <thread>

#include "NBodyParticleSystem.h"
#include "../Materials/Material.h"
#include "Camera.h"

using namespace std;
using namespace glm;

namespace AKOpenGLEngine {

    string NBodyParticleSystem::GetName() const {
        return "NBodyParticleSystem";
    }

    void NBodyParticleSystem::printYaml(YAML::Emitter &out) const {
        ParticleSystem::printYaml(out);
    }

    NBodyParticleSystem::NBodyParticleSystem(Material *material) : ParticleSystem(material) {
        nbodyProblem = new NBodyProblem();
    }

    void NBodyParticleSystem::FixedUpdate(float dt) {
        nbodyProblem->FixedUpdate(dt);
        auto pos = nbodyProblem->getParticlesPositions();
        while (gpuParticles.size() < pos.size()) {
            VertexParticleSystem p;
            gpuParticles.push_back(p);
        }
        for (int i=0; i<pos.size(); i++) {
            gpuParticles[i].position = pos[i];
        }
    }
    
    void NBodyParticleSystem::Render(unsigned int renderPass, Camera *camera, bool useLight) {
        ParticleSystem::Render(renderPass, camera, useLight);
    }
    
    glm::vec3 NBodyParticleSystem::getParticlesCenter() const {
        if(nbodyProblem != nullptr) {
            return nbodyProblem->GetParticlesCenter();
        }
        return vec3(0);
    }

    
    NBodyParticleSystem::~NBodyParticleSystem() {
        if (nbodyProblem != nullptr) {
            delete nbodyProblem;
        }

    }
}
