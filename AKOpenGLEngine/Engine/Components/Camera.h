//
//  Camera.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Camera_H_
#define __Camera_H_

#include <glm/glm.hpp>

#include "Components/Component.h"
#include "Utilities/OpenGL.h"

namespace AKOpenGLEngine {
    
    enum class CameraClearFlag {
        DontClear,
        SolidColor,
        Skybox,
        OnlyDepth
    };
    
    class RenderTexture;
    class CubeMapTexture;
    class Material;
    class Mesh;
    
    class Camera : public Component {
        
    private:
        
#pragma mark Serialized values
        
        CameraClearFlag clearFlag = CameraClearFlag::SolidColor;
        glm::vec4 clearColor = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
        glm::vec4 viewport = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
        RenderTexture *renderTexture = nullptr;
        CubeMapTexture *cubeMapTexture = nullptr;
        Material* customSkyboxMaterial = nullptr;
        float FoV = M_PI_4;
        glm::vec2 clippingPlanes = glm::vec2(0.1f, 1000.0f);
        
#pragma mark Runtime values
        
        mutable glm::mat4 cachedViewMatrix;
        mutable glm::mat4 cachedVPMatrix;
        glm::mat4 projectionMatrix;
        
        static Material* SkyBoxMaterial;
        static Mesh* SkyBoxMesh;
        
#pragma mark Private static methods
        
        static void GenerateSkyboxResources();
        
#pragma mark Private methods
        
        void UpdateCachedMatrices();
        
    public:
        
#pragma mark Constructors
        
        Camera();
        Camera(YAML::Node& initNode);
        
#pragma mark Virtual methods
        
        virtual std::string GetName() const override final;
        virtual void printYaml(YAML::Emitter& out) const override;
        
#pragma mark Getters
        
        CameraClearFlag getCameraClearFlag() const;
        glm::vec4 getCameraClearColor() const;
        glm::mat4 getVPMatrix() const;
        glm::mat4 getProjectionMatrix() const;
        glm::mat4 getViewMatrix() const;
        glm::mat4 getModelMatrix() const;
        
#pragma mark Setters
        
        void setCameraClearFlag(const CameraClearFlag &clearFlag);
        void setCameraClearColor(const glm::vec4 &clearColor);
        void setViewport(const glm::vec4 &viewport);
        void setRenderTexture(RenderTexture *renderTexture);
        void setCubeMapTexture(CubeMapTexture *cubeMapTexture);
        void setCustomSkyboxMaterial(Material *customSkyboxMaterial);
        void setFov(const float &FoV);
        void setClippingPlanes(const glm::vec2 &clippingPlanes);
        
#pragma mark Other methods
        
        void PrepareCameraForRendering();
        void Clear();
    };
}


#endif //__Camera_H_
