//
//  Camera.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <glm/gtc/matrix_transform.hpp>
#include <yaml-cpp/yaml.h>

#include "Components/Camera.h"
#include "GameObject.h"
#include "Components/Transform.h"
#include "Utilities/GLDebug.h"
#include "Managers/ReferenceManager.h"
#include "Managers/RendererManager.h"
#include "Renderer.h"
#include "Materials/RenderTexture.h"
#include "Materials/Shaders/Shaders.h"
#include "Materials/Material.h"
#include "Materials/Shaders/VertexAttrib.h"
#include "Materials/Meshes/Mesh.h"
#include "Materials/Meshes/ExampleMeshes/Meshes.h"

using namespace std;
using namespace glm;
using namespace YAML;

namespace AKOpenGLEngine {
    
    Material* Camera::SkyBoxMaterial;
    Mesh* Camera::SkyBoxMesh;
    
#pragma mark Private static methods
    
    void Camera::GenerateSkyboxResources() {
        Shader *skyboxShader = Shaders::getInstance().GetSkyBoxShader();
        SkyBoxMaterial = new Material(skyboxShader);
        SkyBoxMesh = Meshes::getInstance().GetCubeMesh();
        SkyBoxMesh->FillData();
    }

#pragma mark Private methods

    void Camera::UpdateCachedMatrices() {
        projectionMatrix = perspective(FoV, viewport.z/viewport.w, clippingPlanes.x, clippingPlanes.y);
        if(gameObject != nullptr) {
            Transform *t = gameObject->GetTransform();
            t->setPosition(t->getPosition());
            mat4 translationMatrix = t->getModelMatrix();
            cachedViewMatrix = translationMatrix;
            cachedVPMatrix = projectionMatrix * translationMatrix;
        }
    }
    
#pragma mark Constructors
    
    Camera::Camera() {
        if(SkyBoxMaterial == nullptr) {
            GenerateSkyboxResources();
        }
    }
    
    Camera::Camera(Node &initNode) {
        new(this) Camera();
        
        clearFlag = (CameraClearFlag) initNode["clearFlag"].as<int>();
        FoV = initNode["FoV"].as<float>();
        
        renderTexture = (RenderTexture*)ReferenceManager::getInstance().GetReference(initNode["renderTexture"].as<long long int>());
        cubeMapTexture = (CubeMapTexture*)ReferenceManager::getInstance().GetReference(initNode["cubeMapTexture"].as<long long int>());
        customSkyboxMaterial = (Material*)ReferenceManager::getInstance().GetReference(initNode["customSkyboxMaterial"].as<long long int>());
        
        Node clearColorNode = initNode["clearColor"];
        vec4 v;
        for (int i = 0; i < 4; i++) {
            v[i] = clearColorNode[i].as<float>();
        }
        clearColor = v;
        
        Node viewportNode = initNode["viewport"];
        for (int i = 0; i < 4; i++) {
            v[i] = viewportNode[i].as<float>();
        }
        viewport = v;
        
        Node clippingPlanesNode = initNode["clippingPlanes"];
        for (int i = 0; i < 2; i++) {
            v[i] = clippingPlanesNode[i].as<float>();
        }
        clippingPlanes = vec2(v.x, v.y);
    }
    
#pragma mark Virtual methods
    
    string Camera::GetName() const {
        return "Camera";
    }
    
    void Camera::printYaml(Emitter &out) const {
        Component::printYaml(out);
        out << Key << "clearFlag" << Value << (int) clearFlag;
        out << Key << "clearColor" << Value << clearColor;
        out << Key << "viewport" << Value << viewport;
        out << Key << "renderTexture" << Value << (long long int)renderTexture;
        out << Key << "cubeMapTexture" << Value << (long long int)cubeMapTexture;
        out << Key << "customSkyboxMaterial" << Value << (long long int)customSkyboxMaterial;
        out << Key << "FoV" << Value << FoV;
        out << Key << "clippingPlanes" << Value << clippingPlanes;
    }
    
#pragma mark Getters
    
    CameraClearFlag Camera::getCameraClearFlag() const {
        return clearFlag;
    }
    
    glm::vec4 Camera::getCameraClearColor() const {
        return clearColor;
    }
    
    mat4 Camera::getVPMatrix() const {
        mat4 translationMatrix = gameObject->GetTransform()->getViewMatrix();
        if (cachedViewMatrix != translationMatrix) {
            cachedViewMatrix = translationMatrix;
            cachedVPMatrix = projectionMatrix * translationMatrix;
        }
        return cachedVPMatrix;
    }
    
    mat4 Camera::getProjectionMatrix() const {
        return projectionMatrix;
    }
    
    mat4 Camera::getViewMatrix() const {
        return gameObject->GetTransform()->getViewMatrix();
    }
    
    mat4 Camera::getModelMatrix() const {
        return gameObject->GetTransform()->getModelMatrix();
    }
    
#pragma mark Setters
    
    void Camera::setCameraClearFlag(const CameraClearFlag &clearFlag) {
        this->clearFlag = clearFlag;
    }
    
    void Camera::setCameraClearColor(const vec4 &clearColor) {
        vec4 cc = glm::clamp(clearColor, vec4(0, 0, 0, 0), vec4(1, 1, 1, 1));
        this->clearColor = cc;
    }
    
    void Camera::setViewport(const vec4 &viewport) {
        this -> viewport = viewport;
        UpdateCachedMatrices();
    }
    
    void Camera::setRenderTexture(RenderTexture *renderTexture) {
        this -> renderTexture = renderTexture;
    }
    
    void Camera::setCubeMapTexture(CubeMapTexture *cubeMapTexture) {
        this -> cubeMapTexture = cubeMapTexture;
        SkyBoxMaterial->SetTexture("texture0", (Texture*)cubeMapTexture);
    }
    
    void Camera::setCustomSkyboxMaterial(AKOpenGLEngine::Material *customSkyboxMaterial) {
        this -> customSkyboxMaterial = customSkyboxMaterial;
    }
    
    void Camera::setFov(const float &FoV) {
        this -> FoV = clamp(FoV, 0.0f, (float)M_PI);
        UpdateCachedMatrices();
    }
    
    void Camera::setClippingPlanes(const glm::vec2 &clippingPlanes) {
        vec2 cp = glm::max(vec2(0,0), clippingPlanes);
        if (cp.x>cp.y) {
            swap(cp.x, cp.y);
        }
        this -> clippingPlanes = cp;
        UpdateCachedMatrices();
    }
    
#pragma mark Other methods
    
    void Camera::PrepareCameraForRendering() {
        if (renderTexture  == nullptr) {
            GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, RendererManager::getInstance().getDefaultFramebuffer()));
            GL_CHECK(glViewport(viewport.x, viewport.y, viewport.z, viewport.w));
        } else {
            GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, renderTexture->GetFramebuffer()));
            GLuint width, height;
            renderTexture->GetSizeOfTexture(&width, &height);
            GL_CHECK(glViewport(0, 0, width, height));
        }
        
    }
    
    void Camera::Clear() {
        switch (clearFlag) {
            case CameraClearFlag::DontClear:
                break;
            case CameraClearFlag::SolidColor: {
                GL_CHECK(glClearColor(clearColor.x, clearColor.y, clearColor.z, clearColor.w));
                GL_CHECK(glDepthMask(GL_TRUE));
                GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
                break;
            }
            case CameraClearFlag::Skybox: {
                GL_CHECK(glDepthMask(GL_TRUE));

                GL_CHECK(glClear(GL_DEPTH_BUFFER_BIT));
                Material *mat = customSkyboxMaterial!=nullptr?customSkyboxMaterial:SkyBoxMaterial;
                if (mat == nullptr) {
                    LOG(ERROR) << "Skybox material in camera: " << gameObject->GetName() << " is nullptr";
                    break;
                }
                mat->SetMatrix("ViewMatrix", getProjectionMatrix()*inverse(mat4_cast(GetTransform()->getRotation())) , true, false);
                mat->SetVector("cameraPosition", vec4(GetTransform()->getPosition(), 1), true, false);
                mat->ApplyMaterial();
                SkyBoxMesh->FillData();
                
                GL_CHECK(glCullFace(GL_FRONT));
                GL_CHECK(glDisable(GL_DEPTH_TEST));
                GL_CHECK(glDisable(GL_BLEND));
                GL_CHECK(glDrawElements(SkyBoxMesh->GetDrawType(), SkyBoxMesh->GetIndexDrawSize(), GL_UNSIGNED_INT, 0));
                GL_CHECK(glCullFace(GL_BACK));

                mat->ReleaseMaterial();

                break;
            }
            case CameraClearFlag::OnlyDepth: {
                GL_CHECK(glDepthMask(GL_TRUE));
                GL_CHECK(glClear(GL_DEPTH_BUFFER_BIT));
                break;
            }
        }
        
    }
}
