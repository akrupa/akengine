//
//  Light.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 13.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Light_H_
#define __Light_H_

#include <glm/glm.hpp>

#include "Components/Component.h"

#define MAX_LIGHTS_NUMBER 4

namespace AKOpenGLEngine {

    enum class LightType : int {
        Directional,
        Point,
        Spot
    };

    struct LightStruct {
        LightType type;
        glm::vec3 direction;
        glm::vec3 position;
        float range;
        float spotAngle;
        float intensity;
        glm::vec3 color;
    };

    class Light : public Component {

#pragma mark Serialized values

        mutable LightStruct light;
        
#pragma mark Private methods
        
        void UpdateLightDirection() const;
        void UpdateLightPosition() const;

    public:

#pragma mark Constructors

        Light();

        Light(YAML::Node &initNode);

#pragma mark Getters

        LightType getLightType() const;
        glm::vec3 getLightDirection() const;
        glm::vec3 getLightPosition() const;
        float getLightRange() const;
        float getLightSpotAngle() const;
        float getLightIntensity() const;
        glm::vec3 getLightColor() const;
        LightStruct getLightStruct() const;

#pragma mark Setters

        void setLightType(LightType lightType);
        void setLightRange(float lightRange);
        void setLightSpotAngle(float lightSpotAngle);
        void setLightIntensity(float lightIntensity);
        void setLightColor(glm::vec3 lightColor);

#pragma mark Virtual methods

        virtual std::string GetName() const override final;
        virtual void printYaml(YAML::Emitter &out) const override;
    };
}


#endif //__Light_H_
