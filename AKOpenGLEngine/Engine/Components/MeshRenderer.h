//
//  MeshRenderer.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 25.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __MeshRenderer_H_
#define __MeshRenderer_H_

#include <vector>

#include "Components/Component.h"
#include "Utilities/OpenGL.h"

namespace YAML {
    class Node;
}

namespace AKOpenGLEngine {

    class Mesh;
    class Material;

    class MeshRenderer : public Component {
        
#pragma mark Serialzed values
        
        Mesh *mesh;
        std::vector<Material *>materials;

#pragma mark Runtime values
        
        static unsigned int renderPassHash;
        
#pragma mark Private methods
        
        void SetDepthParameters(unsigned int renderPass);
    
    public:
        
#pragma mark Constructors
        
        MeshRenderer(Mesh* mesh, Material* material);
        MeshRenderer(Mesh* mesh);
        MeshRenderer(YAML::Node node);
        
#pragma mark Getters
        
        void* GetMaterial(unsigned short renderPass) const;
        
#pragma mark Public methods
        
        void AddMaterial(Material* material);
        
#pragma mark Virtual methods
        
        virtual std::string GetName() const override final;
        virtual void Render(unsigned int renderPass, Camera *camera, bool useLight) override;
        virtual void printYaml(YAML::Emitter& out) const override;
    };

}


#endif //__Mesh_H_
