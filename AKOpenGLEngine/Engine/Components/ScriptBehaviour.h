//
//  ScriptBehaviour.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __ScriptBehaviour_H_
#define __ScriptBehaviour_H_

#include "Component.h"

namespace AKOpenGLEngine {

    class ScriptBehaviour : public Component {

    protected:
        ScriptBehaviour();

    };

}


#endif //__ScriptBehaviour_H_
