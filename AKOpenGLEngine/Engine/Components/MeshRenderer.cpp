//
//  MeshRenderer.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 25.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "Components/MeshRenderer.h"
#include "Materials/Meshes/Mesh.h"
#include "Materials/Material.h"
#include "Materials/Shaders/VertexAttrib.h"
#include "Components/Camera.h"
#include "Components/Transform.h"
#include "Utilities/GLDebug.h"
#include "Managers/ReferenceManager.h"
#include "Managers/RendererManager.h"
#include "Materials/Shaders/Shader.h"

using namespace std;
using namespace glm;

namespace AKOpenGLEngine {
    
#pragma mark Runtime values

    unsigned int MeshRenderer::renderPassHash;
    
#pragma mark Private methods

    void MeshRenderer::SetDepthParameters(unsigned int renderPass) {
        if(renderPass == MeshRenderer::renderPassHash)
            return;
        
        MeshRenderer::renderPassHash = renderPass;
        GL_CHECK(glEnable(GL_CULL_FACE));
        //GL_CHECK(glEnable(GL_MULTISAMPLE));
        GL_CHECK(glCullFace(GL_BACK));
        GL_CHECK(glEnable(GL_DEPTH_TEST));
        if (renderPass == 0) {
            GL_CHECK(glDepthMask(GL_TRUE));
            GL_CHECK(glDepthFunc(GL_LEQUAL));
            GL_CHECK(glDisable(GL_BLEND));
        } else {
            GL_CHECK(glDepthMask(GL_FALSE));
            GL_CHECK(glDepthFunc(GL_EQUAL));
            GL_CHECK(glEnable(GL_BLEND));
            GL_CHECK(glBlendFunc(GL_ONE, GL_ONE));
        }
        
    }
    
#pragma mark Constructors
    
    MeshRenderer::MeshRenderer(Mesh *mesh, Material *material) {
        this->mesh = mesh;
        materials.push_back(material);
        
        MeshRenderer::renderPassHash = INT32_MAX;
    }
    
    MeshRenderer::MeshRenderer(Mesh *mesh) {
        this->mesh = mesh;
        MeshRenderer::renderPassHash = INT32_MAX;
    }
    
    MeshRenderer::MeshRenderer(YAML::Node node) {
        Mesh *mesh = (Mesh *) ReferenceManager::getInstance().GetReference(node["mesh"].as<long long int>());
        Material *material = (Material *) ReferenceManager::getInstance().GetReference(node["material"].as<long long int>());
        
        new(this)MeshRenderer(mesh, material);
    }
    
#pragma mark Getters

    
    void *MeshRenderer::GetMaterial(unsigned short renderPass) const {
        if (renderPass >= materials.size()) {
            return nullptr;
        }
        return materials[renderPass];
    }
    
#pragma mark Public methods
    
    void MeshRenderer::AddMaterial(Material *material) {
        materials.push_back(material);
    }
    
#pragma mark Virtual methods

    string MeshRenderer::GetName() const {
        return "MeshRenderer";
    }
    
    void MeshRenderer::Render(unsigned int renderPass, Camera *camera, bool useLight) {
        Component::Render(renderPass, camera, useLight);

        if (renderPass >= materials.size()) {
            return;
        }
        Material *material = materials[renderPass];
        if (mesh == nullptr || material == nullptr) {
            return;
        }
        if (useLight != material->isUsingLight()) {
            return;
        }

        mat4 VP = camera->getVPMatrix();
        mat4 ViewMatrix = camera->getViewMatrix();
        mat4 ModelMatrix = GetTransform()->getModelMatrix();
        
        if (material->IsUsingMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_MVP)) {
            material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_MVP, VP * ModelMatrix);
        }
        if (material->IsUsingMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_MV)) {
            material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_MV, ViewMatrix * ModelMatrix);
        }
        if (material->IsUsingMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_M)) {
            material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_M, ModelMatrix);
        }
        if (material->IsUsingMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_V)) {
            material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_V, ViewMatrix);
        }
        if (material->IsUsingMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_P)) {
            mat4 projectionMatrix = camera->getProjectionMatrix();
            material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_P, projectionMatrix);
        }
        if (material->IsUsingMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_T_MV)) {
            material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_T_MV, transpose(ViewMatrix * ModelMatrix));
        }
        if (material->IsUsingMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_IT_MV)) {
            mat4 NormalMatrix = GetTransform()->getNormalMatrix();
            material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_IT_MV, NormalMatrix);
        }
        if (material->IsUsingMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::NormalMatrix)) {
            mat4 NormalMatrix = GetTransform()->getNormalMatrix();
            material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::NormalMatrix, NormalMatrix);
        }
        /*
        material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_MV, VP * ModelMatrix);
        material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_M, VP * ModelMatrix);
        material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_V, VP * ModelMatrix);
        material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_P, VP * ModelMatrix);
        material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_T_MV, VP * ModelMatrix);
        material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::MATRIX_IT_MV, VP * ModelMatrix);
        material->SetMatrix(SHADER_BUILT_IN_MATRIX_UNIFORM::NormalMatrix, VP * ModelMatrix);
         */
/*
        material->SetMatrix("MATRIX_MVP", VP * ModelMatrix, false, true);
        material->SetMatrix("MATRIX_MV", ModelViewMatrix, false, true);
        material->SetMatrix("MATRIX_M", ModelMatrix, false, true);
        material->SetMatrix("MATRIX_V", ViewMatrix, false, true);
        material->SetMatrix("MATRIX_P", projectionMatrix, false, true);
        material->SetMatrix("MATRIX_VP", VP, false, true);
        material->SetMatrix("MATRIX_T_MV", transpose(ModelViewMatrix), false, true);
        material->SetMatrix("MATRIX_IT_MV", NormalMatrix, false, true);

        material->SetMatrix("NormalMatrix", NormalMatrix, false, true);
 */
        material->ApplyMaterial();

        mesh->FillData();

        if (useLight) {
            GL_CHECK(glBindBufferBase(GL_UNIFORM_BUFFER, (GLuint) material->GetShader()->getUniformLocation("Lights"), RendererManager::getInstance().getLightsBufferObject()));
        }

        SetDepthParameters(renderPass);
        GL_CHECK(glDrawElements(mesh->GetDrawType(), mesh->GetIndexDrawSize(), GL_UNSIGNED_INT, 0));
        material->ReleaseMaterial();
    }

    void MeshRenderer::printYaml(YAML::Emitter &out) const {
        Component::printYaml(out);
        out << YAML::Key << "mesh" << YAML::Value << (long long int) mesh;
        out << YAML::Key << "materials";
        out << YAML::BeginMap;
        for (unsigned long i = 0; i < materials.size(); ++i) {
            Material *material = materials[i];
            out << YAML::Key << i << YAML::Value << (long long int) material;
            ReferenceManager::getInstance().RegisterMaterialReference(material);
        }
        out << YAML::EndMap;
        ReferenceManager::getInstance().RegisterMeshReference(mesh);
    }
}
