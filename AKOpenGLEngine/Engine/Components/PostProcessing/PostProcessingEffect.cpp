//
//  PostProcessingEffect.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 07.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <easylogging++/easylogging++.h>

#include "PostProcessingEffect.h"
#include "../../GameObject.h"
#include "../Camera.h"

namespace AKOpenGLEngine {
    
    PostProcessingEffect::PostProcessingEffect() {
        
    }
    
    void PostProcessingEffect::Start() {
        camera = gameObject->GetComponent<Camera>();
        if (camera == nullptr) {
            LOG(ERROR) << "Cannot find camera component";
        }
    }
}
