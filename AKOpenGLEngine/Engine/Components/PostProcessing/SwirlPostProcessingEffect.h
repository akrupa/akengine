//
//  SwirlPostProcessingEffect.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 07.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __AKOpenGLEngine__SwirlPostProcessingEffect__
#define __AKOpenGLEngine__SwirlPostProcessingEffect__

#include "PostProcessingEffect.h"

namespace AKOpenGLEngine {
    
    class SwirlPostProcessingEffect : public PostProcessingEffect {
        
    };
}

#endif /* defined(__AKOpenGLEngine__SwirlPostProcessingEffect__) */
