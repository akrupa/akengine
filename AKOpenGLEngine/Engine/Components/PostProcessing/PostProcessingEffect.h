//
//  PostProcessingEffect.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 07.06.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __AKOpenGLEngine__PostProcessingEffect__
#define __AKOpenGLEngine__PostProcessingEffect__

#include <memory>

#include "../Component.h"

namespace AKOpenGLEngine {
    
    struct RenderTextureOption {
        int width;
        int height;
    };
    
    class Camera;
    
    class PostProcessingEffect : public Component {
        
        Camera *camera;
        
    public:
        PostProcessingEffect();
        virtual void Start() override;

    };
}

#endif /* defined(__AKOpenGLEngine__PostProcessingEffect__) */
