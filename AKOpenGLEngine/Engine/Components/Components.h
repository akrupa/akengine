//
//  Components.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 08.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Components_H_
#define __Components_H_

#include <yaml-cpp/yaml.h>

#include "../Utilities/Singleton.h"

//TODO: clean class
namespace AKOpenGLEngine {

    class Component;

    class Components : public Singleton<Components> {

    public:
        Component* GetComponent(YAML::Node componentNode);
    };
}


#endif //__Components_H_
