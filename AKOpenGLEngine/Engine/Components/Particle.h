//
//  Particle.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 20.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Particle_H_
#define __Particle_H_

#include <glm/glm.hpp>

namespace AKOpenGLEngine {

    struct Particle {
        glm::vec3 position;
        glm::vec3 velocity;
        glm::vec3 force;
        float mass;
        float lifeTime;
        bool toRemove;
    };
}


#endif //__Point_H_
