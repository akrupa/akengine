//
//  Component.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __Component_H_
#define __Component_H_

#include <string>
#include <vector>

namespace YAML {
    class Emitter;
    class Node;
}

namespace AKOpenGLEngine {

    class GameObject;
    class Transform;
    class Camera;
    class Light;

    class Component {
        
        friend class GameObject;

#pragma Private methods
        
        void AssignGameObject(GameObject* gameObject);
        std::vector<Component *> GetAllComponentsInScene();

    protected:

#pragma mark Protected fields

        bool isUnique = false;
        bool isEnabled = true;
        GameObject *gameObject = nullptr;

#pragma mark Protected constructors
        
        Component();
        Component(GameObject* gameObject);
        
#pragma mark Protected methods
        
        template<typename T>
        T *FindComponent() {
            static_assert(std::is_base_of<Component, T>::value,
                          "T must be a descendant of Component"
                          );
            auto components = GetAllComponentsInScene();
            for (auto &component : components) {
                
                T *t = dynamic_cast<T *> (component);
                if (t != nullptr) {
                    return t;
                }
            }
            return nullptr;
        }
        
        template<typename T>
        std::vector<T> *FindComponents() {
            static_assert(std::is_base_of<Component, T>::value,
                          "T must be a descendant of Component"
                          );
            std::vector<T> v;
            auto components = GetAllComponentsInScene();
            for (auto &component : components) {
                
                T *t = dynamic_cast<T *> (component);
                if (t != nullptr) {
                    v.push_back(t);
                }
            }
            return v;
        }

    public:
        
#pragma mark Virtual methods
        
        virtual ~Component();
        virtual std::string GetName() const = 0;
        virtual void printYaml(YAML::Emitter& out) const;
        virtual void Start();
        virtual void OnDestroy();
        virtual void Update(float dt);
        virtual void FixedUpdate(float dt);
        virtual void Render(unsigned int renderPass, Camera *camera, bool useLight);
        
#pragma mark Public methods

        bool IsEnabled() const;
        void SetActive(bool active);
        Transform* GetTransform() const ;
        
#pragma mark Friend methods

        friend YAML::Emitter& operator << (YAML::Emitter& out, const Component* c);
    };
}

#endif //__Component_H_
