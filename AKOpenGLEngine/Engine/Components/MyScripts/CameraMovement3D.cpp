//
//  CameraMovement3D.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 19.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <yaml-cpp/yaml.h>

#include "CameraMovement3D.h"
#include "../../Managers/InputManager.h"
#include "../Transform.h"

using namespace std;
using namespace glm;
using namespace YAML;

namespace AKOpenGLEngine {

    CameraMovement3D::CameraMovement3D() : ScriptBehaviour() {}

    CameraMovement3D::CameraMovement3D(Node node) {
        new(this)CameraMovement3D();
        cameraSensitivity = node["cameraSensitivity"].as<float>();
        cameraSpeed = node["cameraSpeed"].as<float>();
        rotationX = node["rotationX"].as<float>();
        rotationY = node["rotationY"].as<float>();
    }

    string CameraMovement3D::GetName() const {
        return "CameraMovement3D";
    }

    void CameraMovement3D::Start() {
        ScriptBehaviour::Start();
        lastMousePosition = InputManager::getInstance().GetMousePosition();
    }

    void CameraMovement3D::Update(float dt) {
        ScriptBehaviour::Update(dt);

        ProcessMouse(dt);
        ProcessKeyboard(dt);
    }

    void CameraMovement3D::ProcessMouse(float dt) {
        
        (void)dt;
        vec2 mousePosition = InputManager::getInstance().GetMousePosition();
        vec2 mousePositionDelta = mousePosition - lastMousePosition;
        Transform *transform = GetTransform();

        if (InputManager::getInstance().GetMouseButtonState(MOUSE_BUTTON::LEFT)) {
            rotationX -= mousePositionDelta.x * cameraSensitivity;
            rotationY -= mousePositionDelta.y * cameraSensitivity;
            rotationY = clamp(rotationY, (float) -M_PI_2, (float) M_PI_2);

            quat rot = rotate(quat(), rotationX, vec3(0.0f, 1.0f, 0.0f));
            quat rot2 = rotate(quat(), rotationY, vec3(1.0f, 0.0f, 0.0f));
            transform->setRotation(rot * rot2);
        }
        lastMousePosition = mousePosition;
    
    }

    void CameraMovement3D::ProcessKeyboard(float dt) {
        
        Transform *transform = GetTransform();
        vec3 pos = transform->getPosition();
        vec3 trans = vec3(0);
        quat rot = transform->getRotation();

        float tempCameraSpeed = cameraSpeed;
        if (InputManager::getInstance().GetKeyboardButtonState(KEYBOARD_BUTTON::KEY_LEFT_SHIFT) ||
            InputManager::getInstance().GetKeyboardButtonState(KEYBOARD_BUTTON::KEY_LEFT_SHIFT)) {
            
            tempCameraSpeed *= 100;
        }

        if (InputManager::getInstance().GetKeyboardButtonState(KEYBOARD_BUTTON::KEY_W)) { //w
            trans = rot * vec3(0.0f, 0.0f, -tempCameraSpeed * dt);
        }
        if (InputManager::getInstance().GetKeyboardButtonState(KEYBOARD_BUTTON::KEY_S)) { //s
            trans = rot * vec3(0.0f, 0.0f, tempCameraSpeed * dt);
        }
        if (InputManager::getInstance().GetKeyboardButtonState(KEYBOARD_BUTTON::KEY_A)) { //a
            trans += rot * vec3(-tempCameraSpeed * dt, 0.0f, 0.0f);
        }
        if (InputManager::getInstance().GetKeyboardButtonState(KEYBOARD_BUTTON::KEY_D)) { //d
            trans += rot * vec3(tempCameraSpeed * dt, 0.0f, 0.0f);
        }
        if (InputManager::getInstance().GetKeyboardButtonState(KEYBOARD_BUTTON::KEY_Q)) { //q
            trans += rot * vec3(0.0f, -tempCameraSpeed * dt, 0.0f);
        }
        if (InputManager::getInstance().GetKeyboardButtonState(KEYBOARD_BUTTON::KEY_E)) { //e
            trans += rot * vec3(0.0f, tempCameraSpeed * dt, 0.0f);
        }

        transform->setPosition(pos + trans);
        
    }

    void CameraMovement3D::printYaml(Emitter &out) const {
        ScriptBehaviour::printYaml(out);
        out << Key << "cameraSensitivity" << Value << cameraSensitivity;
        out << Key << "cameraSpeed" << Value << cameraSpeed;
        out << Key << "rotationX" << Value << rotationX;
        out << Key << "rotationY" << Value << rotationY;
    }
}