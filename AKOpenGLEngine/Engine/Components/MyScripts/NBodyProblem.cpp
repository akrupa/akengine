//
//  NBodyProblem.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.07.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <thread>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "NBodyProblem.h"
#include "tipsy.h"

using namespace std;
using namespace glm;

namespace AKNBodyProblem {
    
    void NBodyProblem::OpenFile() {
        if (streamFromFile) {
            outputFile.open("simulation.txt", ios::in | ios::binary);
            char buff[100];
            outputFile.getline(buff, 100);
            sscanf(buff, "%d\n", &particlesCount);
            //outputFile >> particlesCount;
            particles.clear();
            Particle p;
            for (int i=0; i<particlesCount; i++) {
                particles.push_back(p);
            }
        } else {
            outputFile.open("simulation.txt", ios::out | ios::binary);
            outputFile << particlesCount << endl;
        }
    }
    
    void NBodyProblem::SaveStateToFile() {
        //char buffer[100];
        for (auto &particle : particles) {
            outputFile.write((char*)&particle.position, sizeof(vec3));
            //sprintf(buffer, "%f %f %f\n", particle.position.x, particle.position.y, particle.position.z);
            //outputFile.write(buffer, strlen(buffer));
            //outputFile << particle.position.x << " " << particle.position.y << " " << particle.position.z << endl;
        }
    }
    
    void NBodyProblem::CloseFile() {
        outputFile.close();
    }
    
    NBodyProblem::NBodyProblem() {
        Particle p;
/*
        if (!streamFromFile) {
            Particle p;
            for (int i = 0; i < particlesCount; ++i) {
                
                p.mass = 1;
                
                float velocityScale = 1.46;
                float clusterScale = 2.64;
                float scale = clusterScale;
                float vscale = scale * velocityScale;
                float inner = 2.5f * scale;
                float outer = 4.0f * scale;
                
                float x, y, z;
                float len = 2;
                vec3 point;
                
                while (len > 1) {
                    
                    x = rand() / (float) RAND_MAX * 2 - 1;
                    y = rand() / (float) RAND_MAX * 2 - 1;
                    z = rand() / (float) RAND_MAX * 2 - 1;
                    point.x = x;
                    point.y = y;
                    point.z = z;
                    
                    len = length(point);
                }
                p.position = vec3(point.x * (inner + (outer - inner) * rand() / (float) RAND_MAX),
                                  point.y * (inner + (outer - inner) * rand() / (float) RAND_MAX),
                                  point.z * (inner + (outer - inner) * rand() / (float) RAND_MAX));
                
                x = 0.0f; // * (rand() / (float) RAND_MAX * 2 - 1);
                y = 0.0f; // * (rand() / (float) RAND_MAX * 2 - 1);
                z = 1.0f; // * (rand() / (float) RAND_MAX * 2 - 1);
                vec3 axis = vec3(x,y,z);
                axis = normalize(axis);
                
                if (1 - dot(point, axis) < 1e-6)
                {
                    axis.x = point.y;
                    axis.y = point.x;
                    axis = normalize(axis);
                }
                
                vec3 vv = p.position;
                vv = cross(vv, axis);
                
                p.velocity = vec3(vv.x * vscale,
                                  vv.y * vscale,
                                  vv.z * vscale);
                
                
                particles.push_back(p);
            }
        } else {
            Particle p;
            for (int i = 0; i < particlesCount; ++i) {
                particles.push_back(p);
            }
        }
        */
        /*
        vector<vec4> bodyPositions;
        vector<vec4> bodyVelocities;
        vector<int> bodiesIDs;
        int nBodies = 0;
        int nFirst=0, nSecond=0, nThird=0;
        string filename = "Assets/models/galaxy_20K.bin";
        read_tipsy_file<vec4>(bodyPositions, bodyVelocities, bodiesIDs, filename, nBodies, nFirst, nSecond, nThird);
        particles.clear();
        cout<<"read " << nBodies << endl;
        particlesCount = nBodies;
        Particle pp;
        for (int i=0; i<particlesCount; i++) {
            pp.position = vec3(bodyPositions[i]);
            pp.position += vec3(50,0,0);
            pp.velocity = vec3(bodyVelocities[i]);
            pp.mass = bodyPositions[i].w;
            if (pp.mass == 0) {
                pp.mass = 0.0001;
            }
            particles.push_back(pp);
            
            pp.position = vec3(bodyPositions[i]);
            pp.position = rotateX(pp.position, (float)M_PI_2);
            pp.position -= vec3(50,0,0);
            pp.velocity = vec3(bodyVelocities[i]);
            pp.velocity = rotateX(pp.velocity, (float)M_PI_2);
            pp.mass = bodyPositions[i].w;
            if (pp.mass == 0) {
                pp.mass = 0.0001;
            }
            particles.push_back(pp);
        }
        particlesCount *= 2;
        softeningFactor = bodyVelocities[0].w;
        softeningFactorSquared = softeningFactor*softeningFactor;
        */
        OpenFile();
        
        std::vector<OctreeDataClass> blocks;
        
        Octree<OctreeDataClass> octree( Vector3r::ZERO(), 4.0f, 1, 8, 0.00001f );
        
        OctreeAgentBlock agent;
        
        for( int i = 0;  i < 10;  ++i )
        {
            // one in the middle of each octant
            OctreeDataClass o(Vector3r( (rand()%200)/100.0f,(rand()%200)/100.0f,(rand()%200)/100.0f), i);
            blocks.push_back( o);
        }

        
        for(unsigned long i = 0, end = blocks.size();  i < end;  ++i )
        {
            printf("%d: %d\n", i, (int)octree.insert( blocks[i], agent ));
        }

        OctreeVisitorNBody visitor;
        
        octree.visit( visitor );


    }
    
    NBodyProblem::~NBodyProblem() {
        CloseFile();
        cout<<"Closing file"<<endl;
    }
    
    void NBodyProblem::FixedUpdate(float dt) {
        if (streamFromFile) {
            LoadStateFromFile();
        } else {
            UpdateParticles(dt);
            SaveStateToFile();
        }
        //ComputeParticlesCenter();
    }
    
    void NBodyProblem::ComputeParticlesCenter() {
        vec3 sumOfPositions = vec3(0);
        float sumOfMasses = 0.0f;
        for (const auto &particle : particles) {
            sumOfPositions += particle.position * particle.mass;
            sumOfMasses += particle.mass;
        }
        sumOfPositions /= sumOfMasses;
        particlesCenter = sumOfPositions;
    }
    
    void NBodyProblem::LoadStateFromFile() {
        char buf[1000];
        Particle p;
        for (int i=0; i<particlesCount; i++) {
            outputFile.read((char*)&p.position, sizeof(vec3));
            //outputFile.getline(buf, sizeof(buf));
            //sscanf(buf, "%f %f %f", &p.position.x, &p.position.y, &p.position.z);
            //outputFile >> &p.position.x >> p.position.y >> p.position.z;
            particles[i] = p;
        }
    }
    
    glm::vec3 NBodyProblem::GetParticlesCenter() const {
        return particlesCenter;
    }
    
    void NBodyProblem::UpdateParticles(float dt) {
        UpdateEulerODE(dt);
        //UpdateMidpointODE(dt);
        //UpdateRungeKuttaODE(dt);
    }
    
    void NBodyProblem::UpdateEulerODE(float dt) {
        ComputeParticleForces(particles);
        for (unsigned long i = 0; i < particles.size(); ++i) {
            auto particle = particles[i];

            particle.velocity += (particle.force/particle.mass)*dt;
            particle.position += particle.velocity*dt;

            particles[i] = particle;
        }
    }
    
    
    
    void NBodyProblem::UpdateMidpointODE(float dt) {
        auto particlesMidPoint = particles;
        ComputeParticleForces(particlesMidPoint);
        for (unsigned long i = 0; i < particlesMidPoint.size(); ++i) {
            auto particle = particlesMidPoint[i];
            particle.position += particle.velocity*dt*0.5f;
            particle.velocity += (particle.force/particle.mass)*dt*0.5f;
            particlesMidPoint[i] = particle;
        }
        
        ComputeParticleForces(particlesMidPoint);
        for (unsigned long i = 0; i < particles.size(); ++i) {
            auto particleMid = particlesMidPoint[i];
            auto particle = particlesMidPoint[i];
            particle.position += particleMid.velocity*dt;
            particle.velocity += (particleMid.force/particleMid.mass)*dt;
            particles[i] = particle;
        }
    }
    
    void NBodyProblem::UpdateRungeKuttaODE(float dt) {
        auto temp = particles;
        auto k1 = particles;
        auto k2 = particles;
        auto k3 = particles;
        auto k4 = particles;
        
        ComputeParticleForces(temp);
        for (unsigned long i = 0; i < temp.size(); ++i) {
            auto particle = temp[i];
            auto particle2 = temp[i];
            particle.position = particle.velocity*dt;
            particle.velocity = (particle.force/particle.mass)*dt;
            
            particle2.position = particles[i].position + particle.position*0.5f;
            particle2.velocity = particles[i].velocity + particle.velocity*0.5f;
            k1[i] = particle;
            temp[i] = particle2;
        }
        
        ComputeParticleForces(temp);
        for (unsigned long i = 0; i < temp.size(); ++i) {
            auto particle = temp[i];
            auto particle2 = temp[i];
            particle.position = particle.velocity*dt;
            particle.velocity = (particle.force/particle.mass)*dt;
            
            particle2.position = particles[i].position + particle.position*0.5f;
            particle2.velocity = particles[i].velocity + particle.velocity*0.5f;
            k2[i] = particle;
            temp[i] = particle2;
        }
        
        ComputeParticleForces(temp);
        for (unsigned long i = 0; i < temp.size(); ++i) {
            auto particle = temp[i];
            auto particle2 = temp[i];
            particle.position = particle.velocity*dt;
            particle.velocity = (particle.force/particle.mass)*dt;
            
            particle2.position = particles[i].position + particle.position;
            particle2.velocity = particles[i].velocity + particle.velocity;
            k3[i] = particle;
            temp[i] = particle2;
        }
        
        ComputeParticleForces(temp);
        for (unsigned long i = 0; i < temp.size(); ++i) {
            auto particle = temp[i];
            particle.position = particle.velocity*dt;
            particle.velocity = (particle.force/particle.mass)*dt;
            k4[i] = particle;
        }

        for (unsigned long i = 0; i < particles.size(); ++i) {
            auto particle = particles[i];
            particle.position += (k1[i].position+2.0f*k2[i].position+2.0f*k3[i].position+k4[i].position)/6.0f;
            particle.velocity += (k1[i].velocity+2.0f*k2[i].velocity+2.0f*k3[i].velocity+k4[i].velocity)/6.0f;
            particles[i] = particle;
        }
    }

    
    void NBodyProblem::ComputeParticleForces(std::vector<Particle> &particles) {
        
        unsigned concurentThreadsSupported = thread::hardware_concurrency();
        int threadsNumber = std::max(1, (int)concurentThreadsSupported);
        //threadsNumber = 1;
        vector<thread> threads = vector<thread>();
        unsigned int perThread = (unsigned int)particles.size()/threadsNumber;
        
        
        unsigned int *from = new unsigned int [threadsNumber];
        unsigned int *to = new unsigned int [threadsNumber];
        for (int i = 0; i < threadsNumber; ++i) {
            from[i] = i*perThread;
            to[i] = (i+1)*perThread;
            if(i==threadsNumber-1) {
                to[i] = (unsigned int)particles.size();
            }
            threads.push_back(thread(&NBodyProblem::ComputeParticleForcesInThreads, this, ref(particles), ref(from[i]), ref(to[i])));
        }
        for (auto &thread : threads) {
            thread.join();
        }
        delete []from;
        delete []to;
    }
    
    void NBodyProblem::ComputeParticleForcesInThreads(std::vector<Particle> &particles, unsigned int from, unsigned int to) {
        for (unsigned int i = from; i < to; ++i) {
            particles[i].force = vec3(0);
        }
        
        for (unsigned int i = from; i < to; ++i) {
            
            for (unsigned long j = 0; j < particles.size(); ++j) {
                vec3 rij = particles[j].position - particles[i].position;
                float distSquared = glm::length2(rij);
                float denominatorSqrt = 1.0f / sqrt(distSquared + softeningFactorSquared);
                float denominator = denominatorSqrt * denominatorSqrt * denominatorSqrt;
                vec3 f = (rij * particles[j].mass * particles[i].mass) * denominator;
                particles[i].force += f;
            }
            
        }
    }
    
    vector<vec3> NBodyProblem::getParticlesPositions() {
        vector<vec3> pos;
        
        for (int i=0; i<particles.size(); i++) {
            pos.push_back(particles[i].position);
        }
        
        return pos;
    }
}
