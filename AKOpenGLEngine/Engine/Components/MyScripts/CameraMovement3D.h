//
//  CameraMovement3D.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 19.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __CameraMovement3D_H_
#define __CameraMovement3D_H_

#include <glm/glm.hpp>

#include "../ScriptBehaviour.h"

namespace AKOpenGLEngine {

    class CameraMovement3D : public ScriptBehaviour {

        glm::vec2 lastMousePosition;
        float rotationX = 0.0f;
        float rotationY = 0.0f;
        float cameraSensitivity = 0.003f;
        float cameraSpeed = 1.0f;

        void ProcessMouse(float dt);
        void ProcessKeyboard(float dt);

    public:
        CameraMovement3D();
        CameraMovement3D(YAML::Node node);
        virtual std::string GetName() const override final;

        virtual void Start() override;

        virtual void Update(float dt) override;
        virtual void printYaml(YAML::Emitter& out) const override;

    };
}


#endif //__CameraMovement3D_H_
