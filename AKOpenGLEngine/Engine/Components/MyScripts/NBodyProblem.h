//
//  NBodyProblem.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.07.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __AKOpenGLEngine__NBodyProblem__
#define __AKOpenGLEngine__NBodyProblem__

#include <glm/glm.hpp>
#include <vector>
#include <iostream>
#include <fstream>

#include "Octree/Octree.h"

using namespace hxa7241_graphics;

namespace AKNBodyProblem {
    
    class OctreeDataClass {
        
    public:
        OctreeDataClass() : position(), mass() {}
        OctreeDataClass(const Vector3r& position, const float& mass) : position(position), mass(mass) {}
        
        ~OctreeDataClass() {}
        OctreeDataClass( const OctreeDataClass& o) : position(o.position), mass(o.mass) {}
        OctreeDataClass& operator=( const OctreeDataClass& o) {
            if( &o != this )
            {
                position   = o.position;
                mass = o.mass;
            }
            
            return *this;
        }
        
        const Vector3r& getPosition() const { return position; }
        const float& getMass() const { return mass; }
        
    private:
        Vector3r position;
        float mass;

    };
    
    class OctreeAgentBlock : public OctreeAgent<OctreeDataClass> {
        
    public:
        OctreeAgentBlock() {};
        
        virtual ~OctreeAgentBlock() {};
    private:
        OctreeAgentBlock( const OctreeAgentBlock& );
        OctreeAgentBlock& operator=( const OctreeAgentBlock& );

    protected:
        virtual bool  isOverlappingCell ( const OctreeDataClass&    item,
                                         const Vector3r& lowerCorner,
                                         const Vector3r& upperCorner ) const {
            
            const Vector3r& position( item.getPosition() );
            bool isOverlap = true;
            for( int i = 3;  i-- > 0; )
            {
                isOverlap &= (position[i] < upperCorner[i]) &
                             (position[i] > lowerCorner[i]);
            }
            
            return isOverlap;
        }
    };
    
    class OctreeVisitorNBody : public OctreeVisitor<OctreeDataClass> {

    public:
        OctreeVisitorNBody() {}
        
        virtual ~OctreeVisitorNBody() {}
    private:
        OctreeVisitorNBody( const OctreeVisitorNBody& );
        OctreeVisitorNBody& operator=( const OctreeVisitorNBody& );
        
        
        /// commands -------------------------------------------------------------------
        /// octree visitor overrides
    protected:
        virtual void  visitRoot  (const OctreeCell* pRootCell,
                                  const OctreeData& octreeData) {
            
            printf("Visit root\n");
            OctreeRoot::continueVisit( pRootCell, octreeData, *this );

        }
        
        virtual void  visitBranch(const OctreeCell* subCells[8],
                                  const OctreeData& octreeData) {
            
            printf("Visit branch\n");
            
            for( dword i = 0;  i < 8;  ++i )
            {
                OctreeBranch::continueVisit( subCells, octreeData, i, *this );
            }
            
        }
        
        virtual void  visitLeaf  (const Array<const OctreeDataClass*>& items,
                                  const OctreeData& octreeData) {
            
            printf("Visit leaf\n");

        }
    };
    
    struct Particle {
        glm::vec3 position;
        glm::vec3 velocity; 
        glm::vec3 force;
        float mass;
    };
    
    class NBodyProblem {
                
        std::vector<Particle> particles;
        glm::vec3 particlesCenter = glm::vec3(0);
        float softeningFactor = 0.1f;
        float softeningFactorSquared = softeningFactor * softeningFactor;
        unsigned int particlesCount = 4096;
        
        const bool saveToFile = false;
        const bool streamFromFile = true;
        const unsigned int saveEveryXFrames = 1;
        std::fstream outputFile;
        
        void GenerateParticles();

        void UpdateEulerODE(float dt);
        void UpdateMidpointODE(float dt);
        void UpdateRungeKuttaODE(float dt);
        
        void OpenFile();
        void SaveStateToFile();
        void CloseFile();
        
        void UpdateParticles(float dt);
        void LoadStateFromFile();
        
        void ComputeParticlesCenter();
        
        void ComputeParticleForcesInThreads(std::vector<Particle> &particles, unsigned int from, unsigned int to);
        void ComputeParticleForces(std::vector<Particle> &particles);

        
    public:
        NBodyProblem();
        glm::vec3 GetParticlesCenter() const;
        virtual ~NBodyProblem();
        void FixedUpdate(float dt);
        
        std::vector<glm::vec3> getParticlesPositions();
        std::vector<glm::vec3> getParticlesVelocities();
        
        
    };
}

#endif /* defined(__AKOpenGLEngine__NBodyProblem__) */
