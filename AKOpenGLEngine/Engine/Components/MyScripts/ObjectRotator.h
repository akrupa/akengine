//
//  ObjectRotator.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __ObjectRotator_H_
#define __ObjectRotator_H_

#include <glm/glm.hpp>

#include "../ScriptBehaviour.h"

namespace AKOpenGLEngine {

    class ObjectRotator : public ScriptBehaviour {

        glm::vec3 rotationSpeed;

    public:
        ObjectRotator();
        ObjectRotator(YAML::Node node);
        virtual std::string GetName() const override final;

        virtual void Update(float dt) override;
        virtual void printYaml(YAML::Emitter& out) const override;
    };
}


#endif //__ObjectRotator_H_
