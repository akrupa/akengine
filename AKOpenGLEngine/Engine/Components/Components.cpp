//
//  Components.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 08.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <string>

#include "Components.h"
#include "Component.h"
#include "Camera.h"
#include "MyScripts/CameraMovement3D.h"
#include "Transform.h"
#include "MeshRenderer.h"
#include "Light.h"

using namespace std;
using namespace YAML;

namespace AKOpenGLEngine {
    Component* Components::GetComponent(Node componentNode) {
        string componentName = componentNode["name"].as<string>();
        Component *component = nullptr;
        if (componentName == "Camera") {
            component = new Camera(componentNode);
        } else if (componentName == "CameraMovement3D") {
            component = new CameraMovement3D(componentNode);
        } else if (componentName == "Transform") {
            component = new Transform(componentNode);
        } else if (componentName == "MeshRenderer") {
            component = new MeshRenderer(componentNode);
        } else if (componentName == "Light") {
            component = new Light(componentNode);
        }
        return component;
    }
}
