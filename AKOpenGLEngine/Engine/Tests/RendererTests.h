//
//  RendererTests.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 09.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __RendererTests_H_
#define __RendererTests_H_

#include "gtest/gtest.h"

namespace AKOpenGLEngine {
    
    class Renderer;

    class RendererTests : public ::testing::Test {

    protected:

        Renderer* renderer;

        virtual void SetUp() override;
        virtual void TearDown() override;
    };
}


#endif //__RendererTests_H_
