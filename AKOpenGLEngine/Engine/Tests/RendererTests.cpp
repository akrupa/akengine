//
//  RendererTests.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 09.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "Tests/RendererTests.h"
#include "Renderer.h"

namespace AKOpenGLEngine {

    void RendererTests::SetUp() {
        Test::SetUp();
        renderer = new Renderer();
        renderer->CreateDebugContext();
    }

    void RendererTests::TearDown() {
        Test::TearDown();
        renderer->DestroyWindow();
    }
}