//
//  Renderer.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 15.05.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __AKOpenGLEngine__Renderer__
#define __AKOpenGLEngine__Renderer__

#define SDL_MAIN_HANDLED

#include <stdio.h>
#include <SDL2/SDL.h>
#include <vector>

#include "Utilities/Singleton.h"

namespace AKOpenGLEngine {
    
    class Scene;
    
    class Renderer : public Singleton<Renderer> {
        
        SDL_Window *mainwindow;
        SDL_GLContext maincontext;
        
        Scene *currentScene;
        
        int renderFrames = 0;
        Uint32 lastFPSLogTime = 0;
        Uint32 lastUpdateDrawTime;
        Uint32 fixedDeltaTimeToUpdate = 0;
        Uint32 renderClock = 0;
        Uint32 updateClock = 0;
        
        FILE* ffmpeg;
        bool outputVideoToFile = false;
        
        float fixedDeltaTime = 1.0f/30.0f;
        
        void CreatePCWindow();
        void CreateDebugPCContext();
        void CreateMobileWindow();
        
        void UpdateAndDraw();
        void InitLogger();
        
        void ProcessEvent(const SDL_Event * event);
        
    public:
        void GetWindowSize(int *width, int *height);
        void CreateWindow();
        void CreateDebugContext();
        void DestroyWindow();
        void Run();
    };
}
#endif /* defined(__AKOpenGLEngine__Renderer__) */
