#!/bin/bash
DIRECTORY=libraries-osx
mkdir -p $DIRECTORY
cp mainLibsCpp.txt $DIRECTORY/main.cpp
cp CmakeLibs.txt $DIRECTORY/CMakeLists.txt
cd $DIRECTORY
mkdir -p build-osx
cd build-osx
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j
cd ../../
mkdir -p libs/lib/OSX
mkdir -p libs/include
cp $DIRECTORY/libs/lib/*a libs/lib/OSX/
cp -rf $DIRECTORY/libs/include/* libs/include/

cd $DIRECTORY
if [ ! -f SDL2-2.0.3.zip ]; then
  wget https://www.libsdl.org/release/SDL2-2.0.3.zip
fi

if [ ! -d SDL2-2.0.3 ]; then
  unzip SDL2-2.0.3.zip
fi
cd SDL2-2.0.3/Xcode/SDL/
xcodebuild -target "Static Library" -configuration "Release" TARGET_BUILD_DIR="./build" ARCHS=x86_64 ONLY_ACTIVE_ARCH=NO
cd ../../../../
mkdir -p libs/include/SDL2
cp -rf $DIRECTORY/SDL2-2.0.3/include/* libs/include/SDL2/
cp $DIRECTORY/SDL2-2.0.3/Xcode/SDL/build/libSDL2.a libs/lib/OSX/

cd $DIRECTORY
if [ ! -d SDL_image ]; then
  hg clone http://hg.libsdl.org/SDL_image
fi
cd SDL_image/Xcode/
var="$(xcodebuild -showBuildSettings | grep USER_HEADER_SEARCH_PATHS) ../../SDL2-2.0.3/include/"
var="${var/ = /=}"
xcodebuild -target "Static Library" -configuration "Release" TARGET_BUILD_DIR="./build" ARCHS=x86_64 ONLY_ACTIVE_ARCH=NO "$var"
cd ../../../
cp $DIRECTORY/SDL_image/Xcode/build/libSDL2_image.a libs/lib/OSX/
cp -r $DIRECTORY/SDL_image/Xcode/Frameworks/webp.framework libs/lib/OSX/
cp $DIRECTORY/SDL_image/SDL_image.h libs/include/SDL2/

cd $DIRECTORY
if [ ! -d easyloggingpp ]; then
  git clone https://github.com/easylogging/easyloggingpp.git
fi
cd ..

mkdir -p libs/include/easylogging++
cp $DIRECTORY/easyloggingpp/src/easylogging++.h libs/include/easylogging++/
