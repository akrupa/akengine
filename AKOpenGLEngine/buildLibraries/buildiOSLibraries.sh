#!/bin/bash
DIRECTORY=libraries-ios
mkdir -p $DIRECTORY
cp mainLibsCpp.txt $DIRECTORY/main.cpp
cp CmakeLibs.txt $DIRECTORY/CMakeLists.txt
cd $DIRECTORY
mkdir -p build-ios
cd build-ios

if [ ! -d glm ]; then
    git clone https://github.com/g-truc/glm.git
fi
cd glm
git pull
git checkout master
mkdir -p build
cd build
rm -rf *
cmake -DCMAKE_TOOLCHAIN_FILE=../../../../iOS.cmake -DIOS_PLATFORM=SIMULATOR -GXcode -DCMAKE_INSTALL_PREFIX:PATH=${PWD}/../../../libs ..
xcodebuild -target "install" clean build -configuration "Release" TARGET_BUILD_DIR="./build" ONLY_ACTIVE_ARCH=NO
cd ../../

function build {
    if [ ! -d $1 ]; then
      git clone $2
    fi
    cd $1
    git pull
    git checkout $3

    mkdir -p build-os
    cd build-os
    rm -rf *
    cmake -DCMAKE_TOOLCHAIN_FILE=../../../../iOS.cmake -DIOS_PLATFORM=OS -GXcode $4 -DCMAKE_INSTALL_PREFIX:PATH=${PWD}/../../../libs ..
    xcodebuild -target $5 clean build -configuration "Release" TARGET_BUILD_DIR="./build-os" ARCHS='armv7 armv7s arm64' VALID_ARCHS='armv7 armv7s arm64' ONLY_ACTIVE_ARCH=NO -sdk iphoneos
    cd ../

    mkdir -p build-sim
    cd build-sim
    rm -rf *
    cmake -DCMAKE_TOOLCHAIN_FILE=../../../../iOS.cmake -DIOS_PLATFORM=SIMULATOR -GXcode $4 -DCMAKE_INSTALL_PREFIX:PATH=${PWD}/../../../libs ..
    xcodebuild -target $5 clean build -configuration "Release" TARGET_BUILD_DIR="./build-sim" ARCHS='i386 x86_64' VALID_ARCHS='i386 x86_64' ONLY_ACTIVE_ARCH=NO -sdk iphonesimulator
    cd ../

    lipo -create build-sim/lib$1.a build-os/lib$1.a -output ../../libs/lib/lib$1.a
    cd ..
}
build yaml-cpp https://github.com/blair1618/yaml-cpp.git 2703ef784ddc0a5e488d77a7ed8dd8831b64b868 "-DYAML_CPP_BUILD_CONTRIB=OFF -DCMAKE_BUILD_TYPE=release -DBUILD_SHARED_LIBS=OFF -DYAML_CPP_BUILD_TOOLS=OFF" "install"
build assimp https://github.com/assimp/assimp.git v3.1.1 "-DASSIMP_BUILD_ASSIMP_TOOLS=OFF -DCMAKE_BUILD_TYPE=release -DASSIMP_BUILD_SAMPLES=OFF -DASSIMP_BUILD_TESTS=OFF -DASSIMP_INSTALL_PDB=OFF -DBUILD_SHARED_LIBS=OFF" "install"

if [ ! -f SDL2-2.0.3.zip ]; then
    wget https://www.libsdl.org/release/SDL2-2.0.3.zip
fi

if [ ! -d SDL2-2.0.3 ]; then
    unzip SDL2-2.0.3.zip
fi
cd SDL2-2.0.3/Xcode-iOS/SDL/
xcodebuild -target "libSDL" -configuration "Release" TARGET_BUILD_DIR="./build-os" ARCHS='armv7 armv7s arm64' ONLY_ACTIVE_ARCH=NO -sdk iphoneos
xcodebuild -target "libSDL" -configuration "Release" TARGET_BUILD_DIR="./build-sim" ARCHS='i386 x86_64' VALID_ARCHS='i386 x86_64' ONLY_ACTIVE_ARCH=NO -sdk iphonesimulator
lipo -create build-sim/libSDL2.a build-os/libSDL2.a -output ../../../../libs/lib/libSDL2.a
cd ../../
mkdir -p ../../libs/include/SDL2
cp -r include/* ../../libs/include/SDL2/
cd ../../

mkdir -p ../libs/include
cp -r libs/include/* ../libs/include/
mkdir -p ../libs/lib/iOS
cp libs/lib/*.a ../libs/lib/iOS/

if [ ! -d SDL_image ]; then
  hg clone http://hg.libsdl.org/SDL_image
fi
cd SDL_image/Xcode-iOS/
if [[ "$(xcodebuild -showBuildSettings | grep USER_HEADER_SEARCH_PATHS)" == "" ]]; then
  var="USER_HEADER_SEARCH_PATHS = ../../build-ios/SDL2-2.0.3/include/"
else
  var="$(xcodebuild -showBuildSettings | grep USER_HEADER_SEARCH_PATHS) ../../build-ios/SDL2-2.0.3/include/"
fi
var="${var/ = /=}"
xcodebuild -target "libSDL_image" -configuration "Release" TARGET_BUILD_DIR="./build-os" ARCHS='armv7 armv7s arm64' ONLY_ACTIVE_ARCH=NO -sdk iphoneos "$var"
xcodebuild -target "libSDL_image" -configuration "Release" TARGET_BUILD_DIR="./build-sim" ARCHS='i386 x86_64' VALID_ARCHS='i386 x86_64' ONLY_ACTIVE_ARCH=NO -sdk iphonesimulator "$var"
lipo -create build-sim/libSDL2_image.a build-os/libSDL2_image.a -output ../../../libs/lib/iOS/libSDL2_image.a
cd ../../
cp SDL_image/SDL_image.h ../libs/include/SDL2/


if [ ! -d easyloggingpp ]; then
  git clone https://github.com/easylogging/easyloggingpp.git
fi

mkdir -p ../libs/include/easylogging++
cp easyloggingpp/src/easylogging++.h ../libs/include/easylogging++/
