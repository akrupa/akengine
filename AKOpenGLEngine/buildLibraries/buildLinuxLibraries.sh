#!/bin/bash
DIRECTORY=libraries-osx
mkdir -p $DIRECTORY
cp mainLibsCpp.txt $DIRECTORY/main.cpp
cp CmakeLibs.txt $DIRECTORY/CMakeLists.txt
cd $DIRECTORY
mkdir -p build-osx
cd build-osx
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j
cd ../../
mkdir -p ../libs/lib/OSX
mkdir -p ../libs/include
cp $DIRECTORY/libs/lib/*a ../libs/lib/OSX/
cp -rf $DIRECTORY/libs/include/* ../libs/include/

cd $DIRECTORY
if [ ! -f SDL2-2.0.3.zip ]; then
    wget https://www.libsdl.org/release/SDL2-2.0.3.zip
fi

if [ ! -d SDL2-2.0.3 ]; then
    unzip SDL2-2.0.3.zip
fi
cd SDL2-2.0.3/
mkdir -p build
cd build
cmake -DBUILD_SHARED_LIBS=OFF -DCMAKE_BUILD_TYPE=release -DCMAKE_INSTALL_PREFIX:PATH=${PWD}/../../../libs ..
make
cd ../../

if [ ! -d easyloggingpp ]; then
  git clone https://github.com/easylogging/easyloggingpp.git
fi

mkdir -p ../libs/include/easylogging++
cp easyloggingpp/src/easylogging++.h ../libs/include/easylogging++/
